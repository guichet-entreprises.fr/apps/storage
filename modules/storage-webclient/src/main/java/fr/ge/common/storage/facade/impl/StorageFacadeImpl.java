/**
 * 
 */
package fr.ge.common.storage.facade.impl;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;

import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.storage.constant.ICodeExceptionStorageConstant;
import fr.ge.common.storage.facade.IStorageFacade;
import fr.ge.common.storage.utils.StorageTreeUtils;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.model.StoredTrayResult;
import fr.ge.common.storage.ws.model.StoredTrayTree;
import fr.ge.common.storage.ws.rest.IRecordRestService;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;

/**
 * Interface of the facade to call storage ws and handle the errors.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class StorageFacadeImpl implements IStorageFacade {

    private IStorageRestService storageRestService;

    private IRecordStorage recordStorage;

    private IRecordRestService recordRestService;

    /** The functionnal logger. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /**
     * Mutateur sur l'attribut {@link #storageRestService}.
     *
     * @param storageRestService
     *            la nouvelle valeur de l'attribut storageRestService
     */
    public void setStorageRestService(final IStorageRestService storageRestService) {
        this.storageRestService = storageRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #recordRestService}.
     *
     * @param recordRestService
     *            la nouvelle valeur de l'attribut recordRestService
     */
    public void setRecordRestService(final IRecordRestService recordRestService) {
        this.recordRestService = recordRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #recordStorage}.
     *
     * @param recordStorage
     *            la nouvelle valeur de l'attribut recordStorage
     */
    public void setRecordStorage(final IRecordStorage recordStorage) {
        this.recordStorage = recordStorage;
    }

    @Override
    public StoredTrayResult getTray(final String storageTray) throws TechniqueException {

        StoredTrayResult storedTrayResult = null;
        try {
            storedTrayResult = storageRestService.get(storageTray);
        } catch (Exception e) {
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_GET_TRAY);
        }
        if (null == storedTrayResult) {
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_GET_TRAY);
        }
        return storedTrayResult;
    }

    @Override
    public Response download(final String storageId, final String storageTray) throws TechniqueException {
        Response response = null;
        try {
            response = storageRestService.download(storageId, storageTray);
        } catch (Exception e) {
            LOGGER_FONC.error("[ERROR][COMMON STORAGE WEBCLIENT][{}][{}][{}]", storageId, ICodeExceptionStorageConstant.ERR_STORAGE_WS_DOWNLOAD_FILE, e);
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_DOWNLOAD_FILE);
        }
        if (null == response) {
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_DOWNLOAD_FILE);
        }
        return response;
    }

    @Override
    public void remove(String storageId, String storageTray) throws TechniqueException {
        StoredFile storedFile = null;
        try {
            storedFile = storageRestService.remove(storageId, storageTray);
        } catch (Exception e) {
            LOGGER_FONC.error("[ERROR][COMMON STORAGE WEBCLIENT][{}][{}][{}]", storageId, ICodeExceptionStorageConstant.ERR_STORAGE_WS_REMOVE_FILE, e);
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_REMOVE_FILE);
        }
        if (null == storedFile) {
            LOGGER_FONC.error("[ERROR][COMMON STORAGE WEBCLIENT][{}][{}]", storageId, ICodeExceptionStorageConstant.ERR_STORAGE_WS_REMOVE_FILE);
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_REMOVE_FILE);
        }
    }

    @Override
    public StoredTrayTree generateTree(final String storageTray) throws TechniqueException {
        final StoredTrayResult storedTrayResult = this.getTray(storageTray);
        return StorageTreeUtils.generateTree(storedTrayResult);
    }

    /**
     * {@inheritDoc} @throws
     */
    @Override
    public Response export(final String tray, final String uid) throws TechniqueException {
        // -->Get file from tray
        final Response download = this.download(uid, tray);

        if (Status.OK.getStatusCode() != download.getStatus()) {
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_DOWNLOAD_FILE);
        }

        // -->Store file into file system
        try {
            // final byte[] recordAsBytes = IOUtils.toByteArray(recordAsStream);
            final byte[] recordAsBytes = download.readEntity(byte[].class);

            final SpecificationLoader loader = SpecificationLoader.create(new ZipProvider(recordAsBytes));
            this.recordStorage.storeRecord(uid, recordAsBytes, loader.description().getFormUid());

            // -->Remove file from storage
            this.remove(uid, tray);

            return Response.ok().build();

        } catch (TemporaryException e) {
            throw new TechniqueException(ICodeExceptionStorageConstant.ERR_STORAGE_WS_DOWNLOAD_FILE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<StoredFile> search(final SearchQuery query) {
        return this.recordRestService.search(query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders(), query.getSearchTerms());
    }

}
