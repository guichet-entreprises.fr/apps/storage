/**
 * 
 */
package fr.ge.common.storage.constant;

/**
 * Interface ICodeExceptionDashboardConstant.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ICodeExceptionStorageConstant {

  /** Error code exception from storage-server. */
  String ERR_STORAGE_WS = "ERR_STORAGE_WS";

  /** Error code exception from storage-server to remove file from tray. */
  String ERR_STORAGE_WS_REMOVE_FILE = "ERR_STORAGE_WS_REMOVE_FILE";

  /** Error code exception from storage-server to get all files from tray. */
  String ERR_STORAGE_WS_GET_TRAY = "ERR_STORAGE_WS_GET_TRAY";

  /** Error code exception from storage-server to download file. */
  String ERR_STORAGE_WS_DOWNLOAD_FILE = "ERR_STORAGE_WS_DOWNLOAD_FILE";

}
