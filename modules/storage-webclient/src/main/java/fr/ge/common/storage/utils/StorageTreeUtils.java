/**
 * 
 */
package fr.ge.common.storage.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.model.StoredTrayResult;
import fr.ge.common.storage.ws.model.StoredTrayTree;
import fr.ge.core.exception.TechniqueException;

/**
 * Utility tree class for storage-webclient.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class StorageTreeUtils {

    /**
     * 
     * Constructeur de la classe.
     *
     */
    private StorageTreeUtils() {

    }

    /**
     * Static method for tree generation.
     * 
     * @param storedTrayResult
     *            Input storage tray bean
     * @return A storage tray as a tree order by creation date
     * @throws TechniqueException
     *             Technical exception
     */
    public static StoredTrayTree generateTree(final StoredTrayResult storedTrayResult) throws TechniqueException {
        StoredTrayTree result = new StoredTrayTree();
        result.setTree(generateListForTree(storedTrayResult));
        result.setTotalResult(storedTrayResult.getStoredFiles().size());
        result.setTray(storedTrayResult.getTray());
        return result;
    }

    /**
     * Build a new list to display from input tray result.
     * 
     * @param storedTrayResult
     *            Input storage tray bean
     * @return a new generated list as a tree
     */
    private static Map<String, Map<String, List<StoredFile>>> generateListForTree(final StoredTrayResult storedTrayResult) {
        final Map<String, Map<String, List<StoredFile>>> tree = new TreeMap<>(Collections.reverseOrder());

        Collections.sort(storedTrayResult.getStoredFiles(), new Comparator<StoredFile>() {
            @Override
            public int compare(StoredFile o1, StoredFile o2) {
                if (o1.getCreated().before(o2.getCreated())) {
                    return 1;
                }
                return -1;
            }
        });
        storedTrayResult.getStoredFiles().forEach(storedFile -> {

            final LocalDate localDate = Instant.ofEpochMilli(storedFile.getCreated().getTime()) //
                    .atZone(ZoneId.systemDefault()) //
                    .toLocalDate();

            final String year = String.valueOf(localDate.getYear());
            if (!tree.containsKey(year)) {
                tree.put(year, new TreeMap<>(Collections.reverseOrder()));
            }
            if (null != tree.get(year)) {
                final String month = StringUtils.leftPad(String.valueOf(localDate.getMonthValue()), 2, "0");
                Map<String, List<StoredFile>> treeByMonth = tree.get(year);
                if (!treeByMonth.containsKey(month)) {
                    tree.get(year).put(month, new ArrayList<>());
                }
                treeByMonth.get(month).add(storedFile);
            }
        });

        return tree;

        // for (StoredFile storedFile : storedTrayResult.getStoredFiles()) {
        // final Date dateCreation = storedFile.getDateCreation();
        // final String storageId = storedFile.getId();
        // String year = new
        // SimpleDateFormat("YYYY").format(dateCreation).toUpperCase();
        // String month = new
        // SimpleDateFormat("MM").format(dateCreation).toUpperCase();
        // if (!year.equals(currentYear)) {
        // currentYear = year;
        //
        // // -->Construction d'une nouvelle année
        // storedYear = new StoredYear();
        // storedYear.setYear(currentYear);
        //
        // // -->Changement de mois : construction d'un nouveau mois
        // currentMonth = month;
        // storedMonth = new StoredMonth();
        // storedMonth.setMonth(currentMonth);
        //
        // storedMonth.getStorageId().add(storageId);
        // storedYear.getStoredMonth().add(storedMonth);
        //
        // if (!storageList.contains(storedYear)) {
        // storageList.add(storedYear);
        // }
        // } else {
        // if (!month.equals(currentMonth)) {
        //
        // if (!storedYear.getStoredMonth().contains(storedMonth)) {
        // storedYear.getStoredMonth().add(storedMonth);
        // }
        // // -->Changement de mois : construction d'un nouveau mois
        // currentMonth = month;
        // storedMonth = new StoredMonth();
        // storedMonth.setMonth(currentMonth);
        // }
        //
        // // -->Ajout du fichier dans la liste du mois en cours
        // if (!storedMonth.getStorageId().contains(storageId)) {
        // storedMonth.getStorageId().add(storageId);
        // }
        //
        // if (!storedYear.getStoredMonth().contains(storedMonth)) {
        // storedYear.getStoredMonth().add(storedMonth);
        //
        // if (!storageList.contains(storedYear)) {
        // storageList.add(storedYear);
        // }
        // }
        // }
        //
        // }
        // return storageList;
    }
}
