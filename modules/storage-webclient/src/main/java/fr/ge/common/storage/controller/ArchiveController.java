/**
 *
 */
package fr.ge.common.storage.controller;

import java.io.InputStream;
import java.util.Collections;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.ge.common.storage.facade.IStorageFacade;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.web.search.bean.DatatableSearchQuery;
import fr.ge.common.utils.web.search.bean.DatatableSearchResult;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;

/**
 * Generic access to the archives.
 */
@Controller
public class ArchiveController {

    /** The functionnal logger. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /** Storage facade. */
    @Autowired
    private IStorageFacade storageFacade;

    /**
     * Display dashboard home page.
     *
     * @return string
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHome() {
        LOGGER_FONC.debug("Home page display");
        return "redirect:/archive";
    }

    /**
     * Access to the archive page.
     *
     * @param model
     *            the model
     * @return the template name
     */
    @RequestMapping(method = RequestMethod.GET, value = "/archive")
    public String edit(final Model model, @ModelAttribute("tray") final String tray) {
        model.addAttribute("tray", tray);
        return "archive/archive";
    }

    /**
     * Display the elements of a archive.
     * 
     * @param model
     *            the model
     * @param tray
     *            the Id of the archive
     * @return the template name
     */
    @RequestMapping(value = "/archive/{tray}", method = RequestMethod.GET)
    public String display(final Model model, @PathVariable("tray") final String tray) {
        model.addAttribute("tray", tray);
        return "archive/search/main";
    }

    /**
     * {@inheritDoc}
     */
    protected String templatePrefix() {
        return "archive";
    }

    /**
     * jQuery datatable search mapping.
     *
     * @param criteria
     *            jQuery datatable search criteria
     * @param filters
     *            the filters
     * @return search result object
     */
    @RequestMapping(value = "/archive/search/data", method = RequestMethod.GET)
    @ResponseBody
    public DatatableSearchResult<StoredFile> searchData(final DatatableSearchQuery criteria, @QueryParam("filter") final String[] filters, @QueryParam("tray") final String tray) {
        final DatatableSearchResult<StoredFile> datatableSearchResult = new DatatableSearchResult<>(criteria.getDraw());

        final SearchQuery query = new SearchQuery(criteria.getStart(), criteria.getLength());
        query.addOrder("created", "desc");
        query.addFilter("tray", ":", tray);

        if (StringUtils.isNotEmpty(criteria.getSearch().getValue())) {
            query.setSearchTerms(criteria.getSearch().getValue());
        }

        if (null != filters) {
            for (final String filter : filters) {
                query.addFilter(filter);
            }
        }

        // Searching the elements
        final SearchResult<StoredFile> searchResult = this.storageFacade.search(query);

        datatableSearchResult.setRecordsFiltered((int) searchResult.getTotalResults());
        datatableSearchResult.setRecordsTotal((int) searchResult.getTotalResults());
        datatableSearchResult.setData(Optional.ofNullable(searchResult.getContent()).orElse(Collections.emptyList()));

        return datatableSearchResult;
    }

    /**
     * Download a file from STORAGE.
     * 
     * @param modelThe
     *            model
     * @param httpRequest
     *            The request
     * @param httpResponse
     *            The response
     * @param uid
     *            The storage id
     * @param tray
     *            The storage tray
     * @throws Exception
     *             Exception
     */
    @RequestMapping(value = "/archive/tray/{tray}/id/{id}/download", method = RequestMethod.GET)
    public void downloadFile(final Model model, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse, //
            @PathVariable("id") final String uid, //
            @PathVariable("tray") final String tray //
    ) throws Exception {
        LOGGER_FONC.debug("Downloading a storage file {} for {} tray", uid, tray);

        final Response response = this.storageFacade.download(uid, tray);
        final InputStream file = (InputStream) response.getEntity();
        httpResponse.addHeader(HttpHeaders.CONTENT_DISPOSITION, response.getHeaderString(HttpHeaders.CONTENT_DISPOSITION));
        httpResponse.setContentType("application/octet-stream");
        IOUtils.copy(file, httpResponse.getOutputStream());
        httpResponse.flushBuffer();
    }

    /**
     * Delete a file from STORAGE
     * 
     * @param model
     *            The model
     * @param request
     *            The request
     * @param storageId
     *            The storage id
     * @return The page to display
     * @throws TechniqueException
     */
    @RequestMapping(value = "/archive/tray/{tray}/id/{id}/remove", method = RequestMethod.POST)
    public String deleteFile(final Model model, final HttpServletRequest request, final HttpServletResponse httpResponse, //
            @PathVariable("id") final String uid, //
            @PathVariable("tray") final String tray //
    ) throws TechniqueException {
        LOGGER_FONC.debug("Deleting a storage file {} for {} tray", uid, tray);
        try {
            this.storageFacade.remove(uid, tray);
        } catch (TechniqueException te) {
            LOGGER_FONC.error("An error occured when removing file {} from tray '{}' due to : {}", uid, tray, te.getMessage());
            return "error";
        }
        return "redirect:/archive";
    }

    /**
     * Export a file from STORAGE
     * 
     * @param model
     *            The model
     * @param request
     *            The request
     * @param storageId
     *            The storage id
     * @return The page to display
     * @throws TechniqueException
     */
    @RequestMapping(value = "/archive/tray/{tray}/id/{id}/export", method = RequestMethod.POST)
    public String exportFile(final Model model, final HttpServletRequest request, final HttpServletResponse httpResponse, //
            @PathVariable("id") final String uid, //
            @PathVariable("tray") final String tray //
    ) throws TechniqueException {
        LOGGER_FONC.debug("Export a storage file {}", uid);
        try {
            this.storageFacade.export(tray, uid);
        } catch (TechniqueException te) {
            LOGGER_FONC.error("An error occured when removing file {} due to : {}", uid, te.getMessage());
            return "error";
        }
        return "redirect:/archive";
    }
}
