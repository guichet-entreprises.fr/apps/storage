/**
 * 
 */
package fr.ge.common.storage.constant;

/**
 * Class which listing HTTP header JSON keys for X-GE-ACCOUNT.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface IHeaderAccountConstante {

  /**
   * Clé pour la civité de l'utilisateur.
   */
  String USER_GE_ACCOUNT_CIVILITY = "civility";

  /**
   * Clé pour le nom de l'utilisateur.
   */
  String USER_GE_ACCOUNT_NAME = "name";

  /**
   * Clé pour le prenom de l'utilisateur.
   */
  String USER_GE_ACCOUNT_FORNAME = "forName";

  /**
   * Clé pour le email de l'utilisateur.
   */
  String USER_GE_ACCOUNT_MAIL = "mail";

  /**
   * Clé pour le pays de l'utilisateur.
   */
  String USER_GE_ACCOUNT_COUNTRY = "country";

  /**
   * Clé pour le langage de l'utilisateur.
   */
  String USER_GE_ACCOUNT_LANGUAGE = "language";

}
