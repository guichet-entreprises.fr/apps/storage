require([ 'jquery', 'bootstrap', 'lib/polyfill', 'datatables.net' ], function($) {

    $('table[data-toggle="datatable"]').DataTable();

});

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function dropdownTriGE() {
    document.getElementById("dropdownTriGE").classList.toggle("show");
}

function dropdownTriGQ() {
    document.getElementById("dropdownTriGQ").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}