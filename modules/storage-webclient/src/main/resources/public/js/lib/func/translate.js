define([], function() {

    return function(src) {
        var args = Array.prototype.slice.call(arguments, 1);
        return src.replace(/\{([^}]+)\}/g, function(fragment, capture) {
            return args[capture] || fragment;
        });
    };

});
