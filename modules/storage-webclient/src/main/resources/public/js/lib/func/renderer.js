define([ 'handlebars' ], function(Handlebars) {

    Handlebars.registerHelper('is', function (test, value) {
        return test ? value : '';
    });

    Handlebars.registerHelper('not', function (test, value) {
        return !(test) ? value : '';
    });

    return Handlebars.compile;

});
