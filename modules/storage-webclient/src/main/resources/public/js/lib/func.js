/*!
 * Depends:
 *      lib/func/renderer.js
 *      lib/func/duration.js
 *      lib/func/sizeResume.js
 */ 
define([ 'lib/func/renderer', 'lib/func/duration', 'lib/func/sizeResume', 'lib/func/toDate' ], function (renderer, duration, sizeResume, toDate) {
    return {
        renderer: renderer,
        duration: duration,
        sizeResume: sizeResume,
        toDate: toDate
    };
});
