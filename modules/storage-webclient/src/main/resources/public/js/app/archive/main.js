require([ 'jquery', 'lib/i18n', 'lib/loader' ], function($, i18n, loader) {
	
    function showReferential(name) {
        $('#tray').val(name);
        var referentials = $('.referential');
        var referential = $('.referential[data-name="' + name + '"]');
        var uploadForm = $('.upload-form');
        referentials.removeClass('selected');
        referential.addClass('selected');
        if (referential.data('list')) {
            uploadForm.addClass('hidden');
            window.location.replace(referential.data('list'));
        } else {
            uploadForm.removeClass('hidden');
        }
    }

    var routes = {};
    function route(path, fn) {
        var re = '^#' + path + '$';
        routes[re] = {
            re : new RegExp(re),
            fn : fn
        };
    }

    route('show\/(.*)', function(m) {
    	console.log(window.location);
        showReferential(m[1]);
    });

    $(function () {
        $(window).on("hashchange", function(evt) {
            var hash = window.location.hash;
            Object.keys(routes).forEach(function (re) {
                var route = routes[re];
                var m = route.re.exec(hash);
                if (m) {
                    route.fn(m);
                    return false;
                }
            });
        });

        var tray = $('#tray').val();
        if (tray) {
            window.location.hash = 'show/' + tray;
        } 
        
        var url = new URL(window.location.href);
        var tray = url.searchParams.get("tray");;
        var $this = $('#upload'), url = window.location.origin + window.location.pathname +'/'+tray
        if(tray != null){
	        $.ajax(url, {
	            dataType : 'html'
	        }).done(function(html) {
	            $this.html(html);
	        }).fail(function(jqXHR, textStatus, errorThrown) {
	            $this.html(jqXHR.responseText);
	        }).always(function(jqXHR, textStatus) {
	            console.log(textStatus);
	        });
        }

        $('.referential').click(function() {
              window.location.hash = 'show/' + $(this).data('name');
        	   var tray = $(this).data('name');
               var $this = $('#upload'), url = window.location.origin + window.location.pathname +'/'+tray
               $.ajax(url, {
                   dataType : 'html'
               }).done(function(html) {
            	   console.log(this);
                   $this.html(html);
                   $('#archive').DataTable();
                   console.log(html);
               }).fail(function(jqXHR, textStatus, errorThrown) {
                   $this.html(jqXHR.responseText);
               }).always(function(jqXHR, textStatus) {
                   console.log(textStatus);
               });
        });

        $(document).on('click', 'a[role="remove"]', function(evt) {
            var btn = $(this), url = btn.attr('href'), dlg = $('#confirmModal'), uid = btn.data('id');
            evt.preventDefault();
            $('.modal-header .modal-title', dlg).text(i18n('Supprimer une archive {0}', uid));
            dlg.off('click', 'button[role="validate"]').on('click', 'button[role="validate"]', function(evt) {
            	dlg.modal('hide');
            	loader.show();
                $.ajax({
                    url: url,
                    type: 'POST'
                }).always(function() {
                    loader.hide();
                }).done(function(data) {
                    $('#archive').DataTable().ajax.reload();
                }).fail(function(fail){
                	$('#archive').DataTable().ajax.reload();
                });
            }).modal('show');
        }).on('click', 'a[role="export"]', function(evt) {
            var btn = $(this), url = btn.attr('href'), dlg = $('#confirmModal'), uid = btn.data('id');
            evt.preventDefault();
            $('.modal-header .modal-title', dlg).text(i18n('Exporter une archive {0}', uid));
            dlg.off('click', 'button[role="validate"]').on('click', 'button[role="validate"]', function(evt) {
            	dlg.modal('hide');
            	loader.show();
                $.ajax({
                    url: url,
                    type: 'POST'
                }).always(function() {
                    loader.hide();
                }).done(function(data) {
                    $('#archive').DataTable().ajax.reload();
                }).fail(function(fail){
                	$('#archive').DataTable().ajax.reload();
                });
            }).modal('show');
        });
        
    });
});
