package fr.ge.common.storage.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.storage.facade.impl.StorageFacadeImpl;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.model.StoredTrayResult;
import fr.ge.common.storage.ws.model.StoredTrayTree;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.core.exception.TechniqueException;

public class StorageFacadeImplTest extends AbstractTest {

    /** Storage facade */
    @InjectMocks
    private StorageFacadeImpl storageFacade;

    @Mock
    private IStorageRestService storageRestService;

    @Mock
    private IRecordStorage recordStorage;

    /**
     * Injection of mocks in controller.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getTray() throws TechniqueException {
        StoredTrayResult storedTrayExpected = Mockito.mock(StoredTrayResult.class);
        when(storageRestService.get(Mockito.anyString())).thenReturn(storedTrayExpected);
        StoredTrayResult storedTrayResult = storageFacade.getTray("archived");
        assertNotNull(storedTrayResult);
    }

    @Test(expected = TechniqueException.class)
    public void getTrayNull() throws TechniqueException {
        when(storageRestService.get(anyString())).thenReturn(null);
        StoredTrayResult storedTrayResult = storageFacade.getTray("error");
        assertNull(storedTrayResult);
    }

    @Test(expected = TechniqueException.class)
    public void getTrayException() throws TechniqueException {
        when(storageRestService.get(anyString())).thenThrow(new NullPointerException());
        storageFacade.getTray("inferno");
    }

    @Test
    public void download() throws TechniqueException {
        Response responseExpected = Mockito.mock(Response.class);
        when(storageRestService.download(anyString(), anyString())).thenReturn(responseExpected);
        Response response = storageFacade.download("1", "archived");
        assertNotNull(response);
    }

    @Test(expected = TechniqueException.class)
    public void downloadNull() throws TechniqueException {
        when(storageRestService.download(anyString(), anyString())).thenReturn(null);
        storageFacade.download("1", "error");
    }

    @Test(expected = TechniqueException.class)
    public void downloadException() throws TechniqueException {
        when(storageRestService.download(anyString(), anyString())).thenThrow(new NullPointerException());
        storageFacade.download("1", "error");
    }

    @Test
    public void remove() throws TechniqueException {
        StoredFile storedFile = Mockito.mock(StoredFile.class);
        when(storageRestService.remove(anyString(), anyString())).thenReturn(storedFile);
        storageFacade.remove("1", "archived");
    }

    @Test(expected = TechniqueException.class)
    public void removeNull() throws TechniqueException {
        when(storageRestService.remove(anyString(), anyString())).thenReturn(null);
        storageFacade.remove("1", "error");
    }

    @Test(expected = TechniqueException.class)
    public void removeException() throws TechniqueException {
        when(storageRestService.remove(anyString(), anyString())).thenThrow(new NullPointerException());
        storageFacade.remove("1", "error");
    }

    @Test
    public void generateTree() throws TechniqueException {
        StoredTrayResult storedTrayExpected = Mockito.mock(StoredTrayResult.class);
        when(storageRestService.get(anyString())).thenReturn(storedTrayExpected);
        StoredTrayTree storedTrayTreeResult = storageFacade.generateTree("error");
        assertNotNull(storedTrayTreeResult);
    }

    @Test
    public void testExport() throws TechniqueException, TemporaryException {
        final byte[] recordAsBytes = this.resourceAsBytes("export.zip");

        when(storageRestService.download(anyString(), anyString())).thenReturn(Response.ok(recordAsBytes).build());
        doNothing().when(this.recordStorage).storeRecord(anyString(), any(byte[].class), anyString());
        when(storageRestService.remove(anyString(), anyString())).thenReturn(new StoredFile());

        final Response response = storageFacade.export("error", "1");
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        verify(this.storageRestService).download(eq("1"), eq("error"));
        verify(this.recordStorage).storeRecord(eq("1"), eq(recordAsBytes), anyString());
        verify(this.storageRestService).remove(eq("1"), eq("error"));
    }
}
