package fr.ge.common.storage.utils;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.model.StoredTrayResult;
import fr.ge.common.storage.ws.model.StoredTrayTree;

public class StorageTreeUtilsTest {

    /**
     * Convert LocalDate to Date.
     * 
     * @param localDate
     * @return
     */
    private Date localDateToDate(final LocalDate localDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(localDate.getYear(), localDate.getMonthValue() - 1, localDate.getDayOfMonth());
        return calendar.getTime();
    }

    /**
     * Build tree storage item.
     * 
     * @param tray
     * @param uid
     * @param storageFileName
     * @param dateCreated
     * @return
     */
    private StoredFile buildStoredFile(final String tray, final String uid, final String storageFileName, final LocalDate dateCreated) {
        final StoredFile storedFile = new StoredFile();
        storedFile.setCreated(this.localDateToDate(dateCreated));
        storedFile.setId(uid);
        final String path = "/" + tray + "/" + String.valueOf(dateCreated.getYear()) + "/" + String.valueOf(dateCreated.getMonthValue()) + "/" + storageFileName;
        storedFile.setPath(path);
        return storedFile;
    }

    /**
     * Testing generateTree method.
     * 
     * @throws Exception
     */
    @Test
    public void generateTree() throws Exception {
        // -->prepare
        final LocalDate now = LocalDate.now();
        final LocalDate firstDay = now.with(firstDayOfYear());
        final LocalDate lastDay = now.with(lastDayOfYear());
        final String tray = "archived";

        final StoredTrayResult storedTrayResult = new StoredTrayResult();
        storedTrayResult.setTray(tray);
        storedTrayResult.setStoredFiles(new ArrayList<StoredFile>());

        Arrays.asList(firstDay, lastDay).forEach(date -> {
            final String uid = UUID.randomUUID().toString();
            final String name = uid + ".zip";
            storedTrayResult.getStoredFiles().add(this.buildStoredFile(tray, uid, name, date));
        });

        // -->call
        final StoredTrayTree storedTrayTree = StorageTreeUtils.generateTree(storedTrayResult);

        // -->verify
        assertNotNull(storedTrayTree);
        assertNotNull(storedTrayTree.getTree());
        assertNotNull(storedTrayTree.getTree().get(String.valueOf(now.getYear())));
        assertEquals(2, storedTrayTree.getTree().get(String.valueOf(now.getYear())).size());
        assertEquals(2, storedTrayTree.getTotalResult());
    }
}
