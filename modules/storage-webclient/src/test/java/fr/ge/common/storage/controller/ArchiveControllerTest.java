package fr.ge.common.storage.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.storage.facade.IStorageFacade;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class test for Archive controller.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class ArchiveControllerTest extends AbstractTest {

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The mvc. */
    private MockMvc mvc;

    /** Storage facade. */
    @Autowired
    private IStorageFacade storageFacade;

    private static final String STORAGE_UID = "2017-02-ABC-DEF-00";
    private static final String STORAGE_TRAY = "archived";

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.storageFacade);
    }

    /**
     * Test home page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testHome() throws Exception {
        this.mvc.perform(get("/")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/archive"));
    }

    /**
     * Test edit page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEdit() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/archive")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("archive/archive"));
    }

    /**
     * Test display tray page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDisplayTray() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/archive/partners")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("archive/search/main"));
    }

    @Test
    public void testSearchData() throws Exception {
        // prepare
        final SearchResult<StoredFile> sr = new SearchResult<StoredFile>(0, 10);
        sr.setTotalResults(0);
        sr.setStartIndex(0);
        sr.setMaxResults(10);
        sr.setContent(new ArrayList<>());

        final SearchQuery query = new SearchQuery(0, 10);
        query.addFilter("tray", ":", "partners");
        query.addOrder("created", "desc");
        when(this.storageFacade.search(query)).thenReturn(sr);

        // call
        this.mvc.perform(MockMvcRequestBuilders.get("/archive/search/data?tray=partners&draw=1&start=0&length=10")) //
                .andExpect(status().isOk());
    }

    /**
     * Test download file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownload() throws Exception {
        // prepare
        final byte[] asByteArray = "zip form content".getBytes();
        final MockHttpServletRequestBuilder request = get("/archive/tray/{tray}/id/{id}/download", STORAGE_TRAY, STORAGE_UID);

        when(this.storageFacade.download(STORAGE_UID, STORAGE_TRAY))
                .thenReturn(Response.ok(new ByteArrayInputStream(asByteArray)).header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + STORAGE_UID + ".zip").build());

        this.mvc.perform(request) //
                .andExpect(status().isOk()) //
                .andExpect(content().bytes(asByteArray));
    }

    /**
     * Test remove file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemove() throws Exception {
        // prepare
        doNothing().when(this.storageFacade).remove(STORAGE_UID, STORAGE_TRAY);

        // call
        this.mvc.perform(MockMvcRequestBuilders.post("/archive/tray/{tray}/id/{id}/remove", STORAGE_TRAY, STORAGE_UID)) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/archive"));
    }

    /**
     * Test export file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testExport() throws Exception {
        // prepare
        when(this.storageFacade.export(STORAGE_UID, STORAGE_TRAY)).thenReturn(Response.ok().build());

        // call
        this.mvc.perform(MockMvcRequestBuilders.post("/archive/tray/{tray}/id/{id}/export", STORAGE_TRAY, STORAGE_UID)) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/archive"));
    }
}
