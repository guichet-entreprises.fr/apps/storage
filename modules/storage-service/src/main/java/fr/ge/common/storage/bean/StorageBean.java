package fr.ge.common.storage.bean;

import java.util.Date;
import java.util.List;

/**
 * Class representing a stored file.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class StorageBean {

    /** Storage id. **/
    private String id;

    /** Storage original file name. **/
    private String name;

    /** Storage tray. **/
    private String tray;

    /** Storage relative path. **/
    private String path;

    /** Storage id reference. **/
    private String referenceId;

    /** Metadata. **/
    private List<MetaBean> metas;

    /** The created. */
    private Date created;

    /** The updated. */
    private Date updated;

    /**
     * Gets the created.
     *
     * @return the created
     */
    public Date getCreated() {
        return this.created;
    }

    /**
     * Sets the created.
     *
     * @param created
     *            the created
     * @return the t
     */
    public StorageBean setCreated(final Date created) {
        this.created = created;
        return this;
    }

    /**
     * Gets the updated.
     *
     * @return the updated
     */
    public Date getUpdated() {
        return this.updated;
    }

    /**
     * Sets the updated.
     *
     * @param updated
     *            the updated
     * @return the t
     */
    public StorageBean setUpdated(final Date updated) {
        this.updated = updated;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return String id
     */
    public String getId() {
        return id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Accesseur sur l'attribut {@link #name}.
     *
     * @return String originalfilename
     */
    public String getName() {
        return name;
    }

    /**
     * Mutateur sur l'attribut {@link #name}.
     *
     * @param name
     *            la nouvelle valeur de l'attribut originalfilename
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Accesseur sur l'attribut {@link #tray}.
     *
     * @return String tray
     */
    public String getTray() {
        return tray;
    }

    /**
     * Mutateur sur l'attribut {@link #tray}.
     *
     * @param tray
     *            la nouvelle valeur de l'attribut tray
     */
    public void setTray(String tray) {
        this.tray = tray;
    }

    /**
     * Accesseur sur l'attribut {@link #path}.
     *
     * @return String path
     */
    public String getPath() {
        return path;
    }

    /**
     * Mutateur sur l'attribut {@link #path}.
     *
     * @param path
     *            la nouvelle valeur de l'attribut path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Accesseur sur l'attribut {@link #referenceId}.
     *
     * @return String referenceId
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * Mutateur sur l'attribut {@link #referenceId}.
     *
     * @param referenceId
     *            la nouvelle valeur de l'attribut referenceId
     */
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    /**
     * Accesseur sur l'attribut {@link #metas}.
     *
     * @return List<MetaBean> metas
     */
    public List<MetaBean> getMetas() {
        return metas;
    }

    /**
     * Mutateur sur l'attribut {@link #metas}.
     *
     * @param metas
     *            la nouvelle valeur de l'attribut metas
     */
    public void setMetas(List<MetaBean> metas) {
        this.metas = metas;
    }
}
