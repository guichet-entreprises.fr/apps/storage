/**
 *
 */
package fr.ge.common.storage.bean;

/**
 * The Class MetaBean.
 *
 * @author bsadil
 */
public class MetaBean extends DatedBean<MetaBean> {

    /** the key of search. */
    private String key;

    /** the value of key. */
    private String value;

    /** the uid of spec. */
    private String uid;

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key to set
     */
    public MetaBean setKey(final String key) {
        this.key = key;
        return this;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the value to set
     */
    public MetaBean setValue(final String value) {
        this.value = value;
        return this;
    }

    /**
     * Gets the uid.
     *
     * @return the uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * Sets the uid.
     *
     * @param uid
     *            the uid to set
     */
    public MetaBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

}
