/**
 * 
 */
package fr.ge.common.storage.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.storage.bean.StorageBean;
import fr.ge.common.storage.constante.ICodeExceptionConstante;
import fr.ge.common.storage.constante.ICodeMessageTracker;
import fr.ge.common.storage.constante.IStorageConstant;
import fr.ge.common.storage.service.IStorageService;
import fr.ge.common.storage.service.IStorageTrackerService;
import fr.ge.common.storage.service.mapper.SearchMapper;
import fr.ge.common.storage.service.mapper.StorageMapper;
import fr.ge.common.storage.utils.StorageUtils;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.core.exception.TechniqueException;

/**
 * Service managing files into Storage.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class StorageServiceImpl extends AbstractSearchDataServiceImpl<StorageBean> implements IStorageService {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(StorageServiceImpl.class);

    /** Storage mapper. **/
    @Autowired
    private StorageMapper storageMapper;

    /** service to call tracker. */
    @Autowired
    private IStorageTrackerService storageTrackerService;

    /** Dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    @Override
    @Transactional(readOnly = false, value = "ge_storage")
    public StoredFile storeFile(final byte[] storageFileByte, final String storageTray, final String storageFileName, final String storageReferenceId, final String storageRootDirectory,
            final List<SearchQueryFilter> metas) throws TechniqueException {

        // -->Validate JSON Format
        if (null == storageFileByte) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_INVALID_PARAMETER, "Unable to load storage upload file");
        }

        // -->Generate a new track id
        String storageId = null;
        try {
            storageId = this.storageTrackerService.createStorageId();
        } catch (TechniqueException ex) {
            LOGGER_FONC.error(String.format("An error occured when creating a storage identifier to store a file into status %s", storageTray), ex);
            throw ex;
        }
         StorageBean storageBean=null;
         StringBuilder relativeStorageDirectory = null;
        try {
        	//--> generate file path
            final StringBuilder storageDirectory = new StringBuilder();
            storageDirectory.append(storageRootDirectory).append(File.separator);
            relativeStorageDirectory = StorageUtils.getDirectory(storageTray);
            storageDirectory.append(relativeStorageDirectory);
        	
	        // -->Store file (metadata, sha1, md5 file) in local disk
	        final String storageRelativePath = StorageUtils.createStorageFile(storageId, storageFileByte, storageTray, storageFileName, storageReferenceId, storageRootDirectory,relativeStorageDirectory.toString());
	
	        // -->Persistence execution into DB
	        storageBean = this.insertStorageDB(storageId, storageTray, storageFileName, storageReferenceId, storageRelativePath);
	
	        // -->Store metas
	        List<SearchQueryFilter> metasStorage = new ArrayList<>();
	        if (CollectionUtils.isNotEmpty(metas)) {
	            metasStorage.addAll(metas);
	        }
	
	        Optional.ofNullable(storageTray) //
	                .filter(StringUtils::isNotEmpty) //
	                .map(meta -> metasStorage.add(new SearchQueryFilter("tray", meta)));
	
	        Optional.ofNullable(storageReferenceId) //
	                .filter(StringUtils::isNotEmpty) //
	                .map(meta -> metasStorage.add(new SearchQueryFilter("reference", meta)));
	
	        this.storageMapper.insertMetas(storageId, metasStorage);
        }catch (Exception e) {
        	//--> remove file on the file system 
        	final StringBuilder storageDirectory = new StringBuilder();
        	storageDirectory.append(storageRootDirectory).append(File.separator).append(relativeStorageDirectory).append(File.separator).append(storageId);
			
        	if (StringUtils.isNotEmpty(storageFileName)) {
				final Matcher matcher = StorageUtils.getFilePattern().matcher(storageFileName);
				if (matcher.matches()) {
					// -->Get the extension file
					storageDirectory.append(IStorageConstant.POINT).append(matcher.group(1));
				}
			}

        	StorageUtils.remove(storageDirectory.toString());
    
            throw new TechniqueException(e);
        }
        // -->Tracker notification
        String trackerDate = new SimpleDateFormat(IStorageConstant.TRACKER_DATE_PATTERN).format(Calendar.getInstance().getTime());
        this.storageTrackerService.addMessage(storageId, ICodeMessageTracker.STORAGE_FILE_POSTED, storageId, storageTray, trackerDate);

        if (StringUtils.isNotEmpty(storageReferenceId)) {
            this.storageTrackerService.linkReference(storageId, storageReferenceId);
        }

        
        return this.dozer.map(storageBean, StoredFile.class);
    }

    /**
     * Mapping Storage bean to persist into DB.
     * 
     * @param storageSendBean
     *            storage information to persist
     * @param storageRelativePath
     *            storage relative path file
     * @throws TechniqueException
     *             Technical exception
     */
    private StorageBean insertStorageDB(final String storageId, final String storageStatus, final String storageFileName, final String storageReferenceId, final String storageRelativePath)
            throws TechniqueException {
        StorageBean storageBean = new StorageBean();
        try {
            storageBean.setId(storageId);
            storageBean.setName(storageFileName);
            storageBean.setPath(storageRelativePath);
            storageBean.setReferenceId(storageReferenceId);
            storageBean.setTray(storageStatus);
            LOGGER_FONC.debug("Persistence BDD with storage id = [{}]", storageId);
            storageMapper.insertStorage(storageBean);
        } catch (Exception e) {
            String messageBdd = "An error occured when persisting data into database for id " + storageId;
            LOGGER_FONC.error(messageBdd, e);
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_DB_ERROR, messageBdd, e);
        }
        return storageBean;
    }

    @Override
    @Transactional(readOnly = false, value = "ge_storage")
    public StoredFile remove(final String storageId, final String storageTray, final String storageRootDirectory) throws TechniqueException {
        if (null == storageId) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_INVALID_PARAMETER, "Passing null storage id");
        }
        if (null == storageTray) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_INVALID_PARAMETER, "Passing null storage tray");
        }
        final StoredFile storedFile = this.get(storageId, storageTray);

        final StringBuilder storageAbsoluteFilePath = new StringBuilder();
        storageAbsoluteFilePath.append(storageRootDirectory).append(File.separator).append(storedFile.getPath());

        // -->Deleting line from db
        this.storageMapper.deleteStorageByIdAndTray(storageId, storageTray);
        this.storageMapper.deleteMetas(storageId);

        StorageUtils.remove(storageAbsoluteFilePath.toString());

        // -->Tracker notification
        this.storageTrackerService.addMessage(storageId, ICodeMessageTracker.STORAGE_FILE_REMOVED, storageId, storedFile.getTray(),
                new SimpleDateFormat(IStorageConstant.TRACKER_DATE_PATTERN).format(Calendar.getInstance().getTime()));

        return storedFile;
    }

    @Override
    @Transactional(readOnly = true, value = "ge_storage")
    public StoredFile get(final String storageId, final String storageTray) throws TechniqueException {
        if (null == storageId) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_INVALID_PARAMETER, "Passing null storage id");
        }
        if (null == storageTray) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_INVALID_PARAMETER, "Passing null storage tray");
        }

        try {
            return Optional.of(this.storageMapper.getStorageByIdAndTray(storageId, storageTray)) //
                    .filter(s -> null != s) //
                    .map(s -> this.dozer.map(s, StoredFile.class)) //
                    .orElseThrow(() -> new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_DB_ERROR, "Cannot get storage file")) //
            ;
        } catch (Exception e) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_DB_ERROR, "Cannot get storage file");
        }
    }

    @Override
    @Transactional(readOnly = true, value = "ge_storage")
    public List<StoredFile> getAll(final String storageTray) throws TechniqueException {
        if (null == storageTray) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_INVALID_PARAMETER, "Passing null storage tray");
        }

        try {
            return Optional.ofNullable(this.storageMapper.getAll(storageTray)) //
                    .orElse(new ArrayList<>()) //
                    .stream() //
                    .map(s -> this.dozer.map(s, StoredFile.class)) //
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_DB_ERROR, "Cannot get storage files from tray");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchMapper<StorageBean> getMapper() {
        return this.storageMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, value = "ge_storage")
    public StoredFile get(final String storageId) throws TechniqueException {
        if (null == storageId) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_INVALID_PARAMETER, "Passing null storage id");
        }

        try {
            return Optional.of(this.storageMapper.getStorageById(storageId)) //
                    .filter(s -> null != s) //
                    .map(s -> this.dozer.map(s, StoredFile.class)) //
                    .orElseThrow(() -> new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_DB_ERROR, "Cannot get storage file")) //
            ;
        } catch (Exception e) {
            throw new TechniqueException(ICodeExceptionConstante.ERR_STORAGE_DB_ERROR, "Cannot get storage file");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = false, value = "ge_storage")
    public long remove(final List<String> uids, final String fromDate, final List<SearchQueryFilter> filters) {
        final List<SearchQueryFilter> storageFilters = Optional.ofNullable(filters) //
                .filter(f -> null != f && !f.isEmpty()) //
                .map(f -> f) //
                .orElse(new ArrayList<SearchQueryFilter>());
        if (CollectionUtils.isNotEmpty(uids)) {
            storageFilters.add(new SearchQueryFilter("uid", ":", uids));
        }

        if (StringUtils.isNotEmpty(fromDate)) {
            storageFilters.add(new SearchQueryFilter("created<=" + fromDate));
        }

        if (CollectionUtils.isEmpty(storageFilters)) {
            return 0;
        }

        return this.storageMapper.removeAll(storageFilters);
    }
}
