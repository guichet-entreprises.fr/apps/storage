/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.storage.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * UID factory.
 *
 * <p>
 * Generated UID are of the form of "YYYY-MM-XXX-XXX-CC", where :
 * <dl>
 * <dt>YYYY</dt>
 * <dd>year of generation</dd>
 * <dt>MM</dt>
 * <dd>month of generation</dd>
 * <dt>XXX</dt>
 * <dd>random value encoded as uppercase letter</dd>
 * <dt>CC</dt>
 * <dd>checksum</dd>
 * </dl>
 *
 *
 * <p>
 * Checksum is obtained from UID first part and computed as
 * <p>
 * s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
 * <p>
 * where s[i] is the ith character of the UID, n is the first part length (15).
 * Resulting value is reduce to 2 digits by combining digits of string
 * representation of the previous formula.
 *
 * @author Christian Cougourdan
 */
public class UidFactoryImpl {

    /** Number of groups in the part. */
    private static final int NB_PART_GROUPS = 2;

    /** The Constant RNG_ALGORITHM. */
    private static final String RNG_ALGORITHM = "SHA1PRNG";

    /** The Constant DIGITS. */
    private static final char[] DIGITS = "ABCDEFGHJKLMNPQRSTVWXYZ".toCharArray();

    /** The Constant RANDOM_PART_SIZE. */
    private static final int RANDOM_PART_SIZE = 6;

    /** The Constant MAX_VALUE. */
    private static final int MAX_VALUE = ((Double) Math.pow(DIGITS.length, RANDOM_PART_SIZE)).intValue();

    /** The rng. */
    private SecureRandom rng;

    /**
     * Instantiates a new uid factory impl.
     */
    public UidFactoryImpl() {
        // Nothing to do
    }

    /**
     * Generate a new UID.
     *
     * @return UID
     */
    public String uid() {
        final String raw = datePart() + '-' + this.randomPart();

        return raw + '-' + checksum(raw);
    }

    /**
     * Build date part of UID as a formatted string of "YYYY-MM".
     *
     * @return UID date part
     */
    private static String datePart() {
        return DateTime.now().withZone(DateTimeZone.UTC).toString(DateTimeFormat.forPattern("yyyy-MM"));
    }

    /**
     * Build random part of UID as a formatted string of "XXX-XXX".
     *
     * @return UID random part
     */
    private String randomPart() {
        if (null == this.rng) {
            try {
                this.rng = SecureRandom.getInstance(RNG_ALGORITHM);
            } catch (final NoSuchAlgorithmException ex) {
                throw new TechnicalException("Unknown random generator algorithm " + RNG_ALGORITHM, ex);
            }
        }

        final String str = toString(this.rng.nextInt(MAX_VALUE), RANDOM_PART_SIZE);

        return str.substring(0, RANDOM_PART_SIZE / NB_PART_GROUPS) + '-' + str.substring(RANDOM_PART_SIZE / NB_PART_GROUPS);
    }

    /**
     * Generate checksum for specified value, computed as a string of 2
     * characters.
     *
     * Root checksum is obtained from value hashcode.
     *
     * @param value
     *            value to compute
     * @return checksum as string of 2 characters
     */
    private static String checksum(final String value) {
        final String hash = Integer.toString(value.hashCode());

        int checksum = 0;
        for (final char c : hash.toCharArray()) {
            checksum += c - '0';
        }

        return StringUtils.leftPad(Integer.toString(checksum), 2, "0");
    }

    /**
     * Convert integer value to string, padding the result from left to
     * specified size.
     *
     * @param value
     *            value to stringify
     * @param size
     *            minimal length to return
     * @return string representation of specified value, left padded if
     *         necessary
     */
    private static String toString(final int value, final int size) {
        final StringBuilder builder = new StringBuilder();

        int v = value;
        while (v > 0) {
            final int idx = v % DIGITS.length;
            builder.append(DIGITS[idx]);
            v = (v - idx) / DIGITS.length;
        }

        while (builder.length() < size) {
            builder.insert(0, DIGITS[0]);
        }

        return builder.toString();
    }

}
