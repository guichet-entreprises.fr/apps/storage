package fr.ge.common.storage.service.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import fr.ge.common.storage.bean.StorageBean;
import fr.ge.common.storage.ws.model.StorageSendBean;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * {@link StorageSendBean} mapper.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface StorageMapper extends SearchMapper<StorageBean> {

    /**
     * Inserts a file into db.
     * 
     * @param storageSendBean
     *            the storage information
     */
    void insertStorage(@Param("storageBean") final StorageBean storageBean);

    /**
     * Get a tray file from db.
     * 
     * @param storageId
     *            storage uid
     * @param storageTray
     *            storage tray
     * @return Storage bean
     */
    StorageBean getStorageByIdAndTray(@Param("storageId") final String storageId, @Param("storageTray") final String storageTray);

    /**
     * Delete a tray record from db.
     * 
     * @param storageId
     *            Storage uid
     * @param storageTray
     *            storage tray
     */
    void deleteStorageByIdAndTray(@Param("storageId") final String storageId, @Param("storageTray") final String storageTray);

    /**
     * Get all storage tray file from db.
     * 
     * @param storageTray
     *            Storage tray
     * @return List of Storage bean
     */
    List<StorageBean> getAll(String storageTray);

    /**
     * Inserts metas for one storage file.
     * 
     */
    void insertMetas(@Param("uid") final String uid, @Param("metas") final List<SearchQueryFilter> metas);

    /**
     * @param storageId
     */
    void deleteMetas(@Param("uid") final String uid);

    /**
     * Get a file from db.
     * 
     * @param storageId
     *            Storage uid
     * @return Storage bean
     */
    StorageBean getStorageById(String storageId);

    /**
     * Remove records based on filters.
     * 
     * @param filters
     * @return
     */
    long removeAll(@Param("filters") final List<SearchQueryFilter> filters);

}
