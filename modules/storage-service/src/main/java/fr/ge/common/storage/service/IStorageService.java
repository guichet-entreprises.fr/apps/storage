/**
 * 
 */
package fr.ge.common.storage.service;

import java.util.List;

import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.core.exception.TechniqueException;

/**
 * Persistence service for Storge bubble.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface IStorageService extends ISearchDataService {

    /**
     * Method allowing to store a file.
     * 
     * @param storageFile
     *            the byte array
     * @param storageStatus
     *            the storage status
     * @param storageFileName
     *            the storage original file name
     * @param storageReferenceId
     *            the storage existing reference uid
     * @param storageRootDirectory
     *            the storage root directory
     * @return Storage bean
     * @throws TechniqueException
     *             Technical exception
     */
    StoredFile storeFile(final byte[] storageFile, final String storageStatus, final String storageFileName, final String storageReferenceId, final String storageRootDirectory,
            final List<SearchQueryFilter> metas) throws TechniqueException;

    /**
     * Method allowing to remove a file.
     * 
     * @param storageId
     *            Storage uid
     * @param storageTray
     *            Storage tray
     * @param storageRootDirectory
     *            the storage root directory
     * @return Storage bean
     * @throws TechniqueException
     *             Technical exception
     */
    StoredFile remove(final String storageId, final String storageTray, final String storageRootDirectory) throws TechniqueException;

    /**
     * Get a storage file information from db.
     * 
     * @param storageId
     *            Storage uid
     * @param storageTray
     *            Storage tray
     * @return Storage bean
     * @throws TechniqueException
     *             Technical exception
     */
    StoredFile get(final String storageId, final String storageTray) throws TechniqueException;

    /**
     * Get all storage tray file from db.
     * 
     * @param storageTray
     *            Storage tray
     * @return List of Storage bean
     * @throws TechniqueException
     *             Technical exception
     */
    List<StoredFile> getAll(final String storageTray) throws TechniqueException;

    /**
     * @param storageId
     * @return
     */
    StoredFile get(final String storageId) throws TechniqueException;

    /**
     * Remove records based on criteria.
     * 
     * @param uids
     * @param fromDate
     * @param filters
     * @return
     */
    long remove(final List<String> uids, final String fromDate, final List<SearchQueryFilter> filters);
}
