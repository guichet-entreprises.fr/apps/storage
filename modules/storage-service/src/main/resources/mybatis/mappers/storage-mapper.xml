<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="fr.ge.common.storage.service.mapper.StorageMapper">

    <resultMap id="storageBeanMap" type="fr.ge.common.storage.bean.StorageBean">
        <result property="id" column="STORAGEID" />
        <result property="name" column="ORIGINALFILENAME" />
        <result property="tray" column="TRAY" />
        <result property="path" column="PATH" />
        <result property="referenceId" column="REFERENCEID" />
        <result property="created" column="CREATED" javaType="java.util.Date" />
        <result property="updated" column="UPDATED" javaType="java.util.Date" />
        <collection property="metas" ofType="fr.ge.common.storage.bean.MetaBean" resultMap="metaBeanMap" />
    </resultMap>

    <resultMap id="metaBeanMap" type="fr.ge.common.storage.bean.MetaBean">
        <result property="uid" column="UID" />
        <result property="key" column="KEY" />
        <result property="value" column="VALUE" />
    </resultMap>

    <insert id="insertMetas">
        INSERT INTO meta ( id, uid, key, value, created, updated ) VALUES
        <foreach collection="metas" item="meta"
            index="index" open="(" separator="),(" close=")">
            nextval('sq_meta'),
            #{uid},
            #{meta.column},
            #{meta.value},
            now(),
            now()
        </foreach>
    </insert>

    <delete id="deleteMetas">
        DELETE FROM meta WHERE uid = #{uid}
    </delete>

     <insert id="insertStorage" parameterType="fr.ge.common.storage.bean.StorageBean">
        insert into storage (storageId, originalfilename, tray, path, referenceId, created, updated) 
        values (#{storageBean.id}, #{storageBean.name}, #{storageBean.tray}, #{storageBean.path}, #{storageBean.referenceId}, now(), now())
     </insert>

    <select id="getStorageByIdAndTray" statementType="PREPARED" resultMap="storageBeanMap">
        SELECT storageId, originalfilename, tray, path, created, referenceId
        FROM storage
        WHERE storageid = #{storageId} and tray = #{storageTray}
    </select>

    <select id="getStorageById" statementType="PREPARED" resultMap="storageBeanMap">
        SELECT storageId, originalfilename, tray, path, created, referenceId
        FROM storage
        WHERE storageid = #{storageId}
    </select>
    
    <delete id="deleteStorageByIdAndTray">
        DELETE FROM storage WHERE storageid = #{storageId} and tray = #{storageTray}
    </delete>

    <select id="getAll" statementType="PREPARED" resultMap="storageBeanMap">
        SELECT storageId, originalfilename, tray, path, created, referenceId
        FROM storage
        WHERE tray = #{storageTray} order by created desc, storageId
    </select>
    
    <sql id="fragmentSearch">
        SELECT  
        storage.storageId,
        storage.referenceid,
        storage.originalfilename,
        storage.tray,
        storage.path,
        storage.created,
        storage.updated,
        meta.key, 
        meta.value
        FROM storage 
        LEFT JOIN meta 
        ON storage.storageId = meta.uid
    </sql>
    <sql id="fragmentFiltre">
        <include refid="fragmentSearch" />
        <trim prefix="WHERE 1=1 ">
            <foreach collection="filters" item="filter" open="AND (" separator=" AND " close=")">
                <if test='filter.column == "uid"'>
                    <if test='filter.operator == ":"'>
                        storageId IN <foreach collection="filter.values" item="value" open="(" separator=", " close=")">#{value}</foreach>
                    </if>
                    <if test='filter.operator == "~"'>
                        storageId LIKE '%' || UPPER(#{filter.value}) || '%'
                    </if>
                </if>
                <if test='filter.column == "tray"'>
                    <if test='filter.operator == ":"'>
                        tray IN <foreach collection="filter.values" item="value" open="(" separator=", " close=")">#{value}</foreach>
                    </if>
                    <if test='filter.operator == "~"'>
                        tray LIKE '%' || UPPER(#{filter.value}) || '%'
                    </if>
                </if>
                <if test='filter.column == "created"'>
                    <if test='filter.operator == ":"'>
                        storage.created = '${filter.value}'
                    </if>
                    <if test='filter.operator == "&lt;"'>
                        storage.created &lt; '${filter.value}'
                    </if>
                    <if test='filter.operator == "&gt;"'>
                        storage.created &gt; '${filter.value}'
                    </if>
                    <if test='filter.operator == "&lt;="'>
                        storage.created &lt;= '${filter.value}'
                    </if>
                    <if test='filter.operator == "&gt;="'>
                        storage.created &gt;= '${filter.value}'
                    </if>
                </if>
                <if test='filter.column == "searchTerms"'>
                    <if test='filter.operator == ":"'>
                        storageId IN (SELECT m.uid FROM meta m WHERE to_tsvector('french', m.value) @@ to_tsquery('french', '${filter.value}'))
                    </if>
                </if>
            </foreach>
            <if test='searchedValue != null and searchedValue != ""'>
                AND (storageId IN (SELECT m.uid FROM meta m WHERE to_tsvector('french', m.value) @@ to_tsquery('french', '${searchedValue}')) OR storageId LIKE '%' || UPPER('${searchedValue}') || '%')
            </if>
        </trim>
        <trim prefix="ORDER BY ">
            <foreach collection="orders" item="order" open="" separator=", " close="">
                <if test='order.column in { "uid", "created", "tray" }'>
                    <if test='order.column == "uid"'>
                        storageId ${order.order}
                    </if>
                    <if test='order.column == "created"'>
                        storage.created ${order.order}
                    </if>
                    <if test='order.column == "tray"'>
                        tray ${order.order}
                    </if>
                </if>
            </foreach>
        </trim>
    </sql>
    
    <select id="findAll" statementType="PREPARED" resultMap="storageBeanMap">
        <include refid="fragmentFiltre" />
    </select>

    <select id="count" statementType="PREPARED" resultType="long">
        SELECT COUNT(DISTINCT storageId)
        FROM (
            <include refid="fragmentFiltre" />
        ) m
    </select> 

    <sql id="fragmentDelete">
        DELETE FROM storage
    </sql>
    <sql id="fragmentDeleteFiltre">
        <include refid="fragmentDelete" />
        <trim prefix="WHERE 1=1 ">
            <foreach collection="filters" item="filter" open="AND (" separator=" AND " close=")">
                <if test='filter.column == "uid"'>
                    <if test='filter.operator == ":"'>
                        storageId IN <foreach collection="filter.values" item="value" open="(" separator=", " close=")">#{value}</foreach>
                    </if>
                    <if test='filter.operator == "~"'>
                        storageId LIKE '%' || #{filter.value} || '%'
                    </if>
                </if>
                <if test='filter.column == "reference"'>
                    <if test='filter.operator == ":"'>
                        referenceId IN <foreach collection="filter.values" item="value" open="(" separator=", " close=")">#{value}</foreach>
                    </if>
                    <if test='filter.operator == "~"'>
                        referenceId LIKE '%' || #{filter.value} || '%'
                    </if>
                </if>
                <if test='filter.column == "tray"'>
                    <if test='filter.operator == ":"'>
                        tray IN <foreach collection="filter.values" item="value" open="(" separator=", " close=")">#{value}</foreach>
                    </if>
                    <if test='filter.operator == "~"'>
                        tray LIKE '%' || #{filter.value} || '%'
                    </if>
                </if>
                <if test='filter.column == "created"'>
                    <if test='filter.operator == ":"'>
                        date(created) = '${filter.value}'
                    </if>
                    <if test='filter.operator == "&lt;"'>
                        date(created) &lt; '${filter.value}'
                    </if>
                    <if test='filter.operator == "&gt;"'>
                        date(created) &gt; '${filter.value}'
                    </if>
                    <if test='filter.operator == "&lt;="'>
                        date(created) &lt;= '${filter.value}'
                    </if>
                    <if test='filter.operator == "&gt;="'>
                        date(created) &gt;= '${filter.value}'
                    </if>
                </if>
            </foreach>
        </trim>
    </sql>
    <delete id="removeAll" statementType="PREPARED">
        <include refid="fragmentDeleteFiltre" />
    </delete>
</mapper>
