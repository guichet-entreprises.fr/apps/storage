-- Table: meta
DROP SEQUENCE IF EXISTS sq_meta;
DROP TABLE IF EXISTS meta;

CREATE SEQUENCE sq_meta INCREMENT BY 1;
CREATE TABLE meta
(
  id bigint NOT NULL,
  uid character varying(18) NOT NULL,
  key character varying(255) NOT NULL,
  value character varying(255) NOT NULL,
  created timestamp without time zone NOT NULL,
  updated timestamp without time zone NOT NULL,
  tsv tsvector,
  CONSTRAINT meta_pkey PRIMARY KEY (id)
);

CREATE INDEX meta_ts_idx ON meta USING gin (to_tsvector('french'::regconfig, value::text || ' '::text));
CREATE INDEX idx_meta_uid ON meta USING hash(uid);
CREATE INDEX idx_meta_key ON meta USING hash(key);

--Init default meta
WITH DEFAULT_META (uid, meta_key, meta_value) AS (
	SELECT storageid as uid, 'tray' AS meta_key, tray as meta_value FROM storage
	UNION ALL
	SELECT storageid as uid, 'reference' AS meta_key, referenceid as meta_value FROM storage WHERE referenceid IS NOT NULL
)
INSERT INTO meta (id, uid, key, value, created, updated)
SELECT nextval('sq_meta'), uid, meta_key, meta_value, now(), now() FROM DEFAULT_META;

-- ----------------------------------------------
-- Ajout de l'extension unaccent qui permet la recherche de dossiers par texte avec et sans accents
-- ----------------------------------------------
DO
$$BEGIN
   	CREATE TEXT SEARCH CONFIGURATION fr ( COPY = french );
   	ALTER TEXT SEARCH CONFIGURATION fr
	ALTER MAPPING FOR hword, hword_part, word
	WITH french_stem;
EXCEPTION
   WHEN unique_violation THEN
      NULL;  -- ignore error
END;$$;
