-- ----------------------------------------------
-- Constraints on delete cascade for records
-- ----------------------------------------------
--meta
DELETE FROM meta WHERE uid NOT IN (SELECT storageid FROM storage);
ALTER TABLE meta ADD CONSTRAINT fk_meta_uid FOREIGN KEY (uid) REFERENCES "storage" (storageid) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;
