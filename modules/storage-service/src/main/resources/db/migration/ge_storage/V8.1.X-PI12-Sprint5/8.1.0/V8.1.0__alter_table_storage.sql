-- Table: storage
ALTER TABLE storage ADD COLUMN created timestamp;
ALTER TABLE storage ADD COLUMN updated timestamp;

UPDATE storage SET created = datecreation, updated = datecreation;

ALTER TABLE storage DROP COLUMN datecreation;
