DROP INDEX IF EXISTS "idx_storage_created";
CREATE INDEX "idx_storage_created" ON "storage" (created);

DROP INDEX IF EXISTS "idx_storage_storageId";
CREATE INDEX "idx_storage_storageId" ON "storage" (storageId);

DROP INDEX IF EXISTS "idx_storage_tray";
CREATE INDEX "idx_storage_tray" ON "storage" (tray);
