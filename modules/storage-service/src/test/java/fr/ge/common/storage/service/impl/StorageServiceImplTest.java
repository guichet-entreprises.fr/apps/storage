package fr.ge.common.storage.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import fr.ge.common.storage.bean.StorageBean;
import fr.ge.common.storage.service.IStorageService;
import fr.ge.common.storage.service.mapper.StorageMapper;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.test.AbstractDbTest;
import fr.ge.core.exception.TechniqueException;
import fr.ge.tracker.facade.ITrackerFacade;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/gqual-storage-service-config.xml","classpath:spring/test-business-context.xml", 
		"classpath:spring/test-context.xml","classpath:spring/external-services-context.xml"})
public class StorageServiceImplTest extends AbstractDbTest {

    /** The storage service. */
    @Autowired
    private IStorageService storageService;
    
    @Mock
    private StorageMapper storageMapper;
    
    /** tracker facade. */
    @Autowired
    private ITrackerFacade trackerFacade;

    /** String root directory. **/
    private static String ROOT_DIRECTORY = "target/data/storage";

    /** File root directory. **/
    private File rootdirectory = null;

    /**
     * Sets the up. // TODO refaire cette gestion des ressources de test =>
     * création de l'arborescence cible oui, création à la volée des données de
     * tests vide non!
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.initDb(this.resourceName("dataset.xml"));

        this.rootdirectory = new File(ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();
        new File(ROOT_DIRECTORY + File.separator + "archived").mkdirs();
        new File(ROOT_DIRECTORY + File.separator + "archived" + File.separator + "2017").mkdirs();
        new File(ROOT_DIRECTORY + File.separator + "archived" + File.separator + "2017" + File.separator + "01").mkdirs();

        when(this.trackerFacade.post(Mockito.anyString(), Mockito.anyString())).thenReturn("ok");
        when(this.trackerFacade.link(Mockito.anyString(), Mockito.anyString())).thenReturn("ok");
    }

    /**
     * JUNIT for storing file.
     * 
     * @throws TechniqueException
     *             Technical exception
     * @throws IOException
     */
    @Test
    public void storeFile() throws TechniqueException, IOException {
        byte[] storageFile = this.resourceAsBytes("store.zip");

        final String storageTray = "archived";
        final String storageFileName = "Fichier de test.zip";
        final String storageReferenceId = "test1";
        when(this.trackerFacade.createUid()).thenReturn("2019-01-XXX-YYY-01");
        final StoredFile storedFile = this.storageService.storeFile(storageFile, storageTray, storageFileName, storageReferenceId, rootdirectory.getAbsolutePath(), null);
        assertNotNull(storedFile);
        assertNotNull(storedFile.getId());
        assertNotNull(storedFile.getPath());
        assertEquals(storageTray, storedFile.getTray());
    }

    /**
     * JUNIT for storing file.
     * 
     * @throws TechniqueException
     *             Technical exception
     * @throws IOException
     */
    @Test
    public void storeFileErrorDataBase() throws TechniqueException, IOException {
        byte[] storageFile = this.resourceAsBytes("store.zip");

        final String storageTray = "archived";
        final String storageFileName = "Fichier de test.zip";
        final String storageReferenceId = "test1";

        when(this.trackerFacade.createUid()).thenReturn("2019-01-XXX-YYY-01");      
        Mockito.doThrow(new RuntimeException()).when(storageMapper).insertStorage(Mockito.any());
      
        when(this.trackerFacade.createUid()).thenReturn("2019-01-XXX-YYY-01");
        final StoredFile storedFile = this.storageService.storeFile(storageFile, storageTray, storageFileName, storageReferenceId, rootdirectory.getAbsolutePath(), null);
        assertNotNull(storedFile);
        assertNotNull(storedFile.getId());
        assertNotNull(storedFile.getPath());
        assertEquals(storageTray, storedFile.getTray());
    }
    /**
     * JUNIT for storing file with empty reference.
     * 
     * @throws TechniqueException
     *             Technical exception
     * @throws IOException
     */
    @Test
    public void storeFileWithEmptyReference() throws TechniqueException, IOException {
        byte[] storageFile = this.resourceAsBytes("store.zip");

        final String storageTray = "archived";
        final String storageFileName = "Fichier de test.zip";
        final String storageReferenceId = "";

        when(this.trackerFacade.createUid()).thenReturn("2019-01-XXX-YYY-02");

        StoredFile storedFile = this.storageService.storeFile(storageFile, storageTray, storageFileName, storageReferenceId, rootdirectory.getAbsolutePath(), null);
        assertNotNull(storedFile);
        assertNotNull(storedFile.getId());
        assertNotNull(storedFile.getPath());
        assertEquals(storageTray, storedFile.getTray());

        when(this.trackerFacade.createUid()).thenReturn("2019-01-XXX-YYY-03");

        storedFile = this.storageService.storeFile(storageFile, storageTray, storageFileName, null, rootdirectory.getAbsolutePath(), null);
        assertNotNull(storedFile);
        assertNotNull(storedFile.getId());
        assertNotNull(storedFile.getPath());
        assertEquals(storageTray, storedFile.getTray());
    }

    /**
     * JUNIT for storing file with exception.
     * 
     * @throws TechniqueException
     *             Technical exception
     * @throws IOException
     */
    @Test(expected = TechniqueException.class)
    public void storeFileWithNullPointerException() throws TechniqueException, IOException {
        final byte[] storageFile = null;
        final String storageTray = "archived";
        final String storageFileName = "Fichier de test.zip";
        final String storageReferenceId = "test1";

        this.storageService.storeFile(storageFile, storageTray, storageFileName, storageReferenceId, rootdirectory.getAbsolutePath(), null);
        // TODO ajouter commentaire sur la raison du test
    }

    /**
     * JUNIT for getting storage file from db.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test
    public void getStorage() throws TechniqueException {
        assertNotNull(this.storageService.get("2019-01-REC-ORD-01", "archived"));
    }

    /**
     * JUNIT for getting storage file with null storage id.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void getStorageWithNullStorageId() throws TechniqueException {
        assertNotNull(storageService.get(null, "archived"));
    }

    /**
     * JUNIT for getting storage file with null storage tray.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void getStorageWithNullStorageTray() throws TechniqueException {
        assertNotNull(storageService.get("1", null));
    }

    /**
     * JUNIT for getting storage file from db with null..
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void getStorageWithNullPointerException() throws TechniqueException {
        assertNotNull(storageService.get("2017-12-VHB-LKD-55", "archived"));
    }

    /**
     * JUNIT for removing storage file.
     * 
     * @throws TechniqueException
     *             Technical exception
     * @throws IOException
     */
    @Test
    public void remove() throws TechniqueException, IOException {
        String storageId = "2017-12-VHB-LKD-57";
        StorageBean storageBean = new StorageBean();
        storageBean.setId(storageId);
        storageBean.setName("test");
        storageBean.setPath("archived/2017/01/test.zip");
        storageBean.setTray("archived");

        when(this.trackerFacade.createUid()).thenReturn(storageId);

        byte[] storageFile = this.resourceAsBytes("store.zip");
        final String storageTray = "archived";
        final String storageFileName = "Fichier de test.zip";
        final String storageReferenceId = "test1";

        this.storageService.storeFile(storageFile, storageTray, storageFileName, storageReferenceId, rootdirectory.getAbsolutePath(), null);

        assertNotNull(this.storageService.remove(storageId, storageTray, this.rootdirectory.getAbsolutePath()));
    }

    /**
     * JUNIT for getting a storage file with null storage tray.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void getWithNullStorageTray() throws TechniqueException {
        assertNull(storageService.get("1", null));
    }

    /**
     * JUNIT for getting a storage file with null storage id.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void getWithNullStorageId() throws TechniqueException {
        assertNull(storageService.get(null, "archived"));
    }

    /**
     * JUNIT for getting all storage file with null storage tray.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void getAllWithNullStorageTray() throws TechniqueException {
        storageService.getAll(null);
    }

    /**
     * JUNIT for removing a storage file with null storage tray.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void removeWithNullStorageTray() throws TechniqueException {
        assertNull(this.storageService.remove("1", null, rootdirectory.getAbsolutePath()));
    }

    /**
     * JUNIT for removing a storage file with null storage id.
     * 
     * @throws TechniqueException
     *             Technical exception
     */
    @Test(expected = TechniqueException.class)
    public void removeWithNullStorageId() throws TechniqueException {
        assertNull(this.storageService.remove(null, "archived", rootdirectory.getAbsolutePath()));
    }

    /**
     * Test remove uids from storage.
     * 
     */
    @Test
    public void removeUids() {
        List<String> uids = null;
        assertThat(this.storageService.remove(uids, null, null), equalTo(0L));
        uids = Arrays.asList("2019-01-REC-ORD-01");
        assertThat(this.storageService.remove(uids, null, null), equalTo(1L));
    }
//    /**
//     * Initializes objects annotated with Mockito annotations for given testClass:
//     *  &#064;{@link org.mockito.Mock}, &#064;{@link Spy}, &#064;{@link Captor}, &#064;{@link InjectMocks} 
//     * <p>
//     * See examples in javadoc for {@link MockitoAnnotations} class.
//     */
//    public static void initMocks(Object testClass) {
//        if (testClass == null) {
//            throw new MockitoException("testClass cannot be null. For info how to use @Mock annotations see examples in javadoc for MockitoAnnotations class");
//        }
//
//        AnnotationEngine annotationEngine = new GlobalConfiguration().getAnnotationEngine();
//        Class<?> clazz = testClass.getClass();
//
//        //below can be removed later, when we get read rid of deprecated stuff
//        if (annotationEngine.getClass() != new DefaultMockitoConfiguration().getAnnotationEngine().getClass()) {
//            //this means user has his own annotation engine and we have to respect that.
//            //we will do annotation processing the old way so that we are backwards compatible
//            while (clazz != Object.class) {
//                scanDeprecatedWay(annotationEngine, testClass, clazz);
//                clazz = clazz.getSuperclass();
//            }
//        }
//
//        //anyway act 'the new' way
//        annotationEngine.process(testClass.getClass(), testClass);
//    }
//
//    static void scanDeprecatedWay(AnnotationEngine annotationEngine, Object testClass, Class<?> clazz) {
//        Field[] fields = clazz.getDeclaredFields();
//
//        for (Field field : fields) {
//            processAnnotationDeprecatedWay(annotationEngine, testClass, field);
//        }
//    }
//    
//    @SuppressWarnings("deprecation")
//    static void processAnnotationDeprecatedWay(AnnotationEngine annotationEngine, Object testClass, Field field) {
//        boolean alreadyAssigned = false;
//        for(Annotation annotation : field.getAnnotations()) {
//            Object mock = annotationEngine.createMockFor(annotation, field);
//            if (mock != null) {
//                throwIfAlreadyAssigned(field, alreadyAssigned);
//                alreadyAssigned = true;                
//                try {
//                    new FieldSetter(testClass, field).set(mock);
//                } catch (Exception e) {
//                    throw new MockitoException("Problems setting field " + field.getName() + " annotated with "
//                            + annotation, e);
//                }
//            }
//        }
//    }
//
//    static void throwIfAlreadyAssigned(Field field, boolean alreadyAssigned) {
//        if (alreadyAssigned) {
//            new Reporter().moreThanOneAnnotationNotAllowed(field.getName());
//        }
//    }
}
