package fr.ge.common.storage.service.mapper;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.storage.bean.StorageBean;
import fr.ge.common.storage.test.AbstractDbTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class StorageMapperTest extends AbstractDbTest {

    /** The storage mapper. */
    @Autowired
    private StorageMapper storageMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    /**
     * Test inserting a record into db.
     */
    @Test
    public void testInsertStorage() {
        String trackId = "2017-02-GHI-JKM-00";
        String referenceId = "2017-02-GHI-JKM-01";

        StorageBean storageBean = new StorageBean();
        storageBean.setId(trackId);
        storageBean.setName("Fichier de test");
        storageBean.setReferenceId(referenceId);
        storageBean.setTray("archived");
        storageBean.setPath("/archived/2017/02/2017-02-GHI-JKM-00.zip");
        this.storageMapper.insertStorage(storageBean);
    }

    /**
     * Test deleting a record from db.
     */
    @Test
    public void testDeleteStorage() {
        String storageId = "2017-02-GHI-JKM-01";
        String referenceId = "2017-02-GHI-JKM-01";
        String storageTray = "archived";

        StorageBean storageBean = new StorageBean();
        storageBean.setId(storageId);
        storageBean.setName("Fichier de test.zip");
        storageBean.setReferenceId(referenceId);
        storageBean.setTray(storageTray);
        storageBean.setPath("/archived/2017/02/2017-02-GHI-JKM-01.zip");
        this.storageMapper.insertStorage(storageBean);

        storageBean = this.storageMapper.getStorageByIdAndTray(storageId, storageTray);
        assertNotNull(storageBean);

        this.storageMapper.deleteStorageByIdAndTray(storageId, storageTray);
    }

    /**
     * Test getting all records from db.
     */
    @Test
    public void testGetAll() {
        assertNotNull(this.storageMapper.getAll("archived"));
    }
}
