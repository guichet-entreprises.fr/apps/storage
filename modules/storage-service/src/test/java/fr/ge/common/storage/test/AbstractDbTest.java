package fr.ge.common.storage.test;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class AbstractDbTest.
 *
 * @author aolubi
 */
public abstract class AbstractDbTest {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDbTest.class);

  /** The data source. */
  @Autowired
  private DataSource dataSource;

  /**
   * Inits the db.
   *
   * @param dataSetResourceNames
   *          the data set resource names
   * @throws Exception
   *           the exception
   */
  @Transactional
  protected void initDb(final String... dataSetResourceNames) throws Exception {
    LOGGER.info("Starting data initialization....");

    final DataSourceDatabaseTester databaseTester = new DataSourceDatabaseTester(this.dataSource);
    final List < IDataSet > dataSets = new ArrayList<>();
    for (final String dataSetResourceName : dataSetResourceNames) {
      final ReplacementDataSet dataSet = new ReplacementDataSet(
        new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream(dataSetResourceName)));
      dataSet.addReplacementObject("[NULL]", null);
      dataSets.add(dataSet);
    }

    databaseTester.setDataSet(new CompositeDataSet(dataSets.toArray(new IDataSet[] {})));
    databaseTester.onSetup();

    LOGGER.info("Data initialization done.");
  }

  /**
   * Resource name.
   *
   * @param name
   *          the name
   * @return the string
   */
  protected String resourceName(final String name) {
    return String.format("%s-%s", this.getClass().getSimpleName(), name);
  }

}
