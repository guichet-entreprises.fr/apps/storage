package fr.ge.common.storage.service.impl;


import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import fr.ge.common.storage.constante.IStorageConstant;
import fr.ge.common.storage.service.IStorageService;
import fr.ge.common.storage.service.mapper.StorageMapper;
import fr.ge.common.storage.utils.StorageUtils;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.core.exception.TechniqueException;
import fr.ge.tracker.facade.ITrackerFacade;

/*Test du web service StorageServiceImpl sans accès à la base de donnée*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-context2.xml","classpath:spring/test-persistence-context2.xml","classpath:spring/external-services-context.xml","classpath:spring/gqual-storage-service-config.xml","classpath:spring/test-business-context.xml"})
public class StorageServiceImplTest2 extends AbstractTest {

    /** The storage service. */
    @Autowired
    private IStorageService storageService;
    
    @Autowired
    private StorageMapper storageMapper;
    
    /** tracker facade. */
    @Autowired
    private ITrackerFacade trackerFacade;

    /** String root directory. **/
    private static String ROOT_DIRECTORY = "target/data/storage";

    /** File root directory. **/
    private File rootdirectory = null;

    /**
     * Sets the up. // TODO refaire cette gestion des ressources de test =>
     * création de l'arborescence cible oui, création à la volée des données de
     * tests vide non!
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {

        this.rootdirectory = new File(ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();
        when(this.trackerFacade.post(Mockito.anyString(), Mockito.anyString())).thenReturn("ok");
        when(this.trackerFacade.link(Mockito.anyString(), Mockito.anyString())).thenReturn("ok");
    }



    /**
     * JUNIT for storing file.
     * 
     * @throws TechniqueException
     *             Technical exception
     * @throws IOException
     */
@Test
    public void storeFileErrorDataBase() throws TechniqueException, IOException {
      
    	byte[] storageFile = this.resourceAsBytes("store.zip");
        final String storageTray = "archived";
        final String storageFileName = "Fichier de test.zip";
        final String storageReferenceId = "test1";
        when(this.trackerFacade.createUid()).thenReturn("2019-01-XXX-YYY-01");      
        Mockito.doThrow(new RuntimeException()).when(storageMapper).insertStorage(Mockito.any());
        try {
        this.storageService.storeFile(storageFile, storageTray, storageFileName, storageReferenceId, ROOT_DIRECTORY, null);
        }catch (Exception e) {
			// TODO: handle exception
		}
		StringBuilder relativeStorageDirectory1 = StorageUtils.getDirectory(storageTray);
		String storageDirectoryFileZip =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip";
		String storageDirectoryFileSha =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip"+IStorageConstant.SHA1_EXTENSION;
		String storageDirectoryFileMd5 =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip"+IStorageConstant.MD5_EXTENSION;
		String storageDirectoryFileTxt =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip"+IStorageConstant.TXT_EXTENSION;
		
		
		assertFalse(Paths.get(storageDirectoryFileZip).toFile().exists());
		assertFalse(Paths.get(storageDirectoryFileSha).toFile().exists());
		assertFalse(Paths.get(storageDirectoryFileMd5).toFile().exists());
		assertFalse(Paths.get(storageDirectoryFileTxt).toFile().exists());

    }
    
@Test
public void storeFileErrorDataBase2() throws TechniqueException, IOException {
  
	byte[] storageFile = this.resourceAsBytes("store.zip");
    final String storageTray = "archived";
    final String storageFileName = "Fichier de test.zip";
    final String storageReferenceId = "test1";
    when(this.trackerFacade.createUid()).thenReturn("2019-01-XXX-YYY-01");      
    Mockito.doNothing().when(storageMapper).insertStorage(Mockito.any());
    Mockito.doThrow(new RuntimeException()).when(storageMapper).insertMetas(Mockito.any(), Mockito.any());
    try {
    this.storageService.storeFile(storageFile, storageTray, storageFileName, storageReferenceId, ROOT_DIRECTORY, null);
    }catch (Exception e) {
		// TODO: handle exception
	}
	StringBuilder relativeStorageDirectory1 = StorageUtils.getDirectory(storageTray);
	String storageDirectoryFileZip =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip";
	String storageDirectoryFileSha =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip"+IStorageConstant.SHA1_EXTENSION;
	String storageDirectoryFileMd5 =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip"+IStorageConstant.MD5_EXTENSION;
	String storageDirectoryFileTxt =ROOT_DIRECTORY + File.separator + relativeStorageDirectory1 + File.separator  +"2019-01-XXX-YYY-01.zip"+IStorageConstant.TXT_EXTENSION;
	
	
	assertFalse(Paths.get(storageDirectoryFileZip).toFile().exists());
	assertFalse(Paths.get(storageDirectoryFileSha).toFile().exists());
	assertFalse(Paths.get(storageDirectoryFileMd5).toFile().exists());
	assertFalse(Paths.get(storageDirectoryFileTxt).toFile().exists());

}
    
}
