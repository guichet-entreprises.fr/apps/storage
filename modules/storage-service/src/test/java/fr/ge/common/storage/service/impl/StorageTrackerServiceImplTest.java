package fr.ge.common.storage.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.storage.bean.MessageTrackerProvider;
import fr.ge.common.storage.service.IStorageTrackerService;
import fr.ge.core.exception.TechniqueException;
import fr.ge.tracker.facade.ITrackerFacade;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/gqual-storage-service-config.xml", "classpath:spring/test-business-context.xml", "classpath:spring/external-services-context.xml",
        "classpath:spring/test-context.xml" })
public class StorageTrackerServiceImplTest {

    /** The service. */
    @Autowired
    private IStorageTrackerService storageTrackerService;

    /** tracker facade. */
    @Autowired
    private ITrackerFacade trackerFacade;

    /**
     * Message Provider.
     */
    @Autowired
    private MessageTrackerProvider messageTrackerProvider;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * Test creating new track id.
     * 
     * @throws TechniqueException
     *             Exceptiion raised
     */
    @Test
    public void testCreateStorageId() throws TechniqueException {
        String trackId = "2017-02-ABC-DEF-00";
        when(trackerFacade.createUid()).thenReturn(trackId);
        String actual = storageTrackerService.createStorageId();
        assertEquals(actual, trackId);
    }

    /**
     * Test creating new track id with exception.
     * 
     * @throws TechniqueException
     *             Exceptiion raised
     */
    @Test
    public void testFaileCreateTrackId() throws TechniqueException {
        doThrow(new NullPointerException()).when(trackerFacade).createUid();
        assertNotNull(storageTrackerService.createStorageId());
    }

    /**
     * Test add a new message.
     * 
     */
    @Test
    public void testAddMessage() {
        String trackId = "2017-02-ABC-DEF-00";
        String eventCode = "tracker.storage.posted";
        when(messageTrackerProvider.getMessage(Matchers.anyString(), Matchers.anyString())).thenReturn("");
        storageTrackerService.addMessage(trackId, eventCode);
    }

    /**
     * Test add a new message with failure.
     * 
     */
    @Test
    public void testFailedAddMessage() {
        String trackId = "2017-02-ABC-DEF-00";
        String eventCode = "";
        when(trackerFacade.post(Matchers.anyString(), Matchers.anyString())).thenThrow(new NullPointerException());
        storageTrackerService.addMessage(trackId, eventCode);
    }

    /**
     * Test link method.
     * 
     * @throws TechniqueException
     *             technical exception
     * 
     */
    @Test
    public void testLink() throws TechniqueException {
        String storageId = "2017-02-ABC-DEF-00";
        String referenceId = "2017-02-ABC-DEF-01";
        when(trackerFacade.link(Matchers.anyString(), Matchers.anyString())).thenReturn(storageId);
        storageTrackerService.linkReference(storageId, referenceId);
    }

}
