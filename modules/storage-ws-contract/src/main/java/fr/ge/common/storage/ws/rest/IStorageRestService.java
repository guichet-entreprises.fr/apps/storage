/**
 * 
 */
package fr.ge.common.storage.ws.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.model.StoredTrayResult;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Path("/v1")
public interface IStorageRestService {

    /**
     * REST method storing a file with status.
     * 
     * @param storageTray
     *            the deposit tray : archived, error or inferno
     * @param storageFile
     *            the file to store
     * @param storageFilename
     *            the storage filename
     * @param storageReferenceId
     *            the id to reference the file to
     * @return The response
     */
    @POST
    // @Path("/tray/{storageTray : (archived)|(error)|(inferno) }/file")
    @Path("tray/{storageTray}/file")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des archives", //
            summary = "Archiver un dossier dans une pile spécifique.", //
            description = "Archivage d'un dossier dans une pile spécifique." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_OCTET_STREAM //
                    ) //
            ), //
    })
    Response store( //
            @Parameter(description = "Pile de dépôt : (archived, error, inferno ou partners)") @PathParam("storageTray") final String storageTray, //
            @Parameter(description = "Contenu de l'archive") final Attachment storageFile, //
            @Parameter(description = "Nom de l'archive") @QueryParam("storageFilename") final String storageFilename,
            @Parameter(description = "Référence de l'archive") @QueryParam("storageReferenceId") final String storageReferenceId, //
            @Parameter(description = "Métadonnées de l'archive") @QueryParam("metas") final List<SearchQueryFilter> metas);

    /**
     * REST method downloading a file.
     * 
     * @param storageTray
     *            the deposit tray : archived, error or inferno
     * @param storageId
     *            The uid storage
     * @return The response
     */
    @GET
    @Path("tray/{storageTray}/file/{storageId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation( //
            tags = "Service de gestion des archives", //
            summary = "Télécharger une archive déposée dans une pile spécifique.", //
            description = "Téléchargement d'une archive déposée dans une pile spécifique." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_OCTET_STREAM //
                    ) //
            ), //
    })
    Response download( //
            @Parameter(description = "Identifiant technique de l'archive") @PathParam("storageId") @DefaultValue("") final String storageId, //
            @Parameter(description = "Pile de dépôt : (archived, error, inferno ou partners)") @PathParam("storageTray") final String storageTray);

    /**
     * REST method removing a file.
     * 
     * @param storageTray
     *            the deposit tray : archived, error or inferno
     * @param storageId
     *            The uid storage
     * @return The response
     */
    @DELETE
    @Path("tray/{storageTray}/file/{storageId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des archives", //
            summary = "Supprimer une archive déposée dans une pile spécifique.", //
            description = "Suppression d'une archive déposée dans une pile spécifique." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_OCTET_STREAM //
                    ) //
            ), //
    })
    StoredFile remove( //
            @Parameter(description = "Identifiant technique de l'archive") @PathParam("storageId") @DefaultValue("") final String storageId, //
            @Parameter(description = "Pile de dépôt : (archived, error, inferno ou partners)") @PathParam("storageTray") final String storageTray);

    /**
     * REST method getting all tray records.
     * 
     * @param storageTray
     *            the deposit tray : archived, error or inferno
     * @return The response
     */
    @GET
    @Path("tray/{storageTray}")
    @Produces({ MediaType.APPLICATION_JSON })
    @Operation( //
            tags = "Service de gestion des archives", //
            summary = "Visualiser le contenu d'une pile.", //
            description = "Visualisation le contenu d'une pile." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_OCTET_STREAM //
                    ) //
            ), //
    })
    StoredTrayResult get( //
            @Parameter(description = "Pile de dépôt : (archived, error, inferno ou partners)") @PathParam("storageTray") final String storageTray);

    @POST
    @Path("tray/{storageTray}/file/{storageId}/export")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des archives", //
            summary = "Exporter un dossier vers le support pour traitement.", //
            description = "Téléchargement puis export du dossier avec suppression de l'archive dans une pile spécifique." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response export( //
            @Parameter(description = "Pile de dépôt : (archived, error, inferno ou partners)") @PathParam("storageTray") final String storageTray, //
            @Parameter(description = "Identifiant Storage") @PathParam("storageId") final String storageId);
}
