/**
 *
 */
package fr.ge.common.storage.ws.model;

/**
 * The Class StorageMetaBean
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class StorageMetaBean {

    /** the key of search. */
    private String key;

    /** the value of key. */
    private String value;

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key to set
     */
    public StorageMetaBean setKey(final String key) {
        this.key = key;
        return this;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the value to set
     */
    public StorageMetaBean setValue(final String value) {
        this.value = value;
        return this;
    }
}
