/**
 * 
 */
package fr.ge.common.storage.ws.rest;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Storage record REST service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Path("/v1/Record")
public interface IRecordRestService {

    /**
     * Search for records.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return issues search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Rechercher des dossiers", //
            description = "Recherche full-text de dossiers à partir des critères de tri et filtre" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<StoredFile> search( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Mots-clés") @QueryParam("q") String searchTerms //
    );

    /**
     * REST method downloading a file.
     * 
     * @param storageId
     *            The uid storage
     * @return The response
     */
    @GET
    @Path("/code/{storageId}/download")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Télécharger une archive à partir d'un identifiant", //
            description = "Téléchargement d'une archive à partir d'un identifiant" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_OCTET_STREAM //
                    ) //
            ), //
    })
    Response download( //
            @Parameter(description = "Identifiant technique") @PathParam("storageId") @DefaultValue("") final String storageId);

    /**
     * Remove records based on criteria.
     *
     * @param code
     *            the record identifier
     * @return the response
     */
    @DELETE
    @Path("/remove")
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Supprimer des archives à partir de critères de filtre", //
            description = "Suppression en masse des archives à partir de critères de filtre" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ), //
    })
    Response remove( //
            @Parameter(description = "Storage identifiers") @QueryParam("uids") final List<String> uids, //
            @Parameter(description = "Date from as yyyy-MM-dd") @QueryParam("fromDate") final String fromDate, //
            @Parameter(description = "Query filters") @QueryParam("filters") final List<SearchQueryFilter> filters);

}
