/**
 * 
 */
package fr.ge.common.storage.ws.rest.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.storage.constante.ICodeMessageTracker;
import fr.ge.common.storage.constante.IStorageConstant;
import fr.ge.common.storage.service.IStorageService;
import fr.ge.common.storage.service.IStorageTrackerService;
import fr.ge.common.storage.ws.bean.conf.StorageConfigurationBean;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.model.StoredTrayResult;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Storage Service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api("Storage REST services")
@Path("/v1")
public class StorageRestServiceImpl implements IStorageRestService {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /** The storage service. */
    @Autowired
    private IStorageService storageService;

    /** The storage tracker service. */
    @Autowired
    private IStorageTrackerService storageTrackerService;

    @Autowired
    private StorageConfigurationBean storageConfigurationBean = StorageConfigurationBean.getInstance();

    @Autowired
    private IRecordStorage recordStorage;

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("tray/{storageTray}/file")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Record a file into Storage")
    @ApiImplicitParams({ @ApiImplicitParam(name = "storageUploadFile", value = "Storage upload file", required = true, dataType = "java.io.File", paramType = "form"),
            @ApiImplicitParam(name = "storageTray", value = "Storage tray", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "storageReferenceUid", value = "Storage reference uid", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "storageFilename", value = "Storage original filename", required = false, dataType = "string", paramType = "form") })
    public Response store(@PathParam("storageTray") final String storageTray, final Attachment storageFile, @QueryParam("storageFilename") final String storageFilename,
            @QueryParam("storageReferenceId") final String storageReferenceId, @QueryParam("metas") final List<SearchQueryFilter> metas) {
        StoredFile storedFile = null;
        try {
            if (!isStatusValid(storageTray)) {
                return Response.status(400).build();
            }
            byte[] resourceAsBytes = storageFile.getObject() instanceof byte[] ? (byte[]) storageFile.getObject() : storageFile.getObject(byte[].class);
            if (ArrayUtils.isEmpty(resourceAsBytes)) {
                return Response.serverError().entity("Record content is empty").build();
            }

            storedFile = this.storageService.storeFile(resourceAsBytes, storageTray, storageFilename, storageReferenceId, this.storageConfigurationBean.getStorageRootDirectory(), metas);
        } catch (TechniqueException ex) {
            LOGGER_FONC.error(String.format("An error occured when storing a file into status %s", storageTray), ex);
            return Response.noContent().build();
        }

        return Response.ok(storedFile).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).build();
    }

    @Override
    @DELETE
    @Path("tray/{storageTray}/file/{storageId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Remove a file from Storage")
    public StoredFile remove(@ApiParam("Storage id") @PathParam("storageId") final String storageId, @ApiParam("Storage tray") @PathParam("storageTray") final String storageTray) {
        StoredFile storedFile = null;
        try {
            if (!isStatusValid(storageTray)) {
                LOGGER_FONC.warn("try {storageTray} don't existe", storageTray);
                return null;
            }
            storedFile = this.storageService.remove(storageId, storageTray, this.storageConfigurationBean.getStorageRootDirectory());
        } catch (TechniqueException e) {
            LOGGER_FONC.error("an error occurred while deleting the archive {} => : {}", storageId, e);
            return null;
        }
        return storedFile;
    }

    @Override
    @GET
    @Path("tray/{storageTray}/file/{storageId}")
    @ApiOperation(value = "Download a file from Storage")
    public Response download(@ApiParam("Storage Id") @PathParam("storageId") final String storageId, @ApiParam("Storage tray") @PathParam("storageTray") final String storageTray) {
        StoredFile storedFile = null;
        try {
            if (!isStatusValid(storageTray)) {
                return Response.status(400).build();
            }
            storedFile = this.storageService.get(storageId, storageTray);
        } catch (Exception e) {
            return Response.noContent().build();
        }

        String fileName = storedFile.getId();
        if (StringUtils.isNotBlank(storedFile.getName())) {
            fileName = storedFile.getName();
        }

        try {
            String trackerDate = new SimpleDateFormat(IStorageConstant.TRACKER_DATE_PATTERN).format(Calendar.getInstance().getTime());
            this.storageTrackerService.addMessage(storageId, ICodeMessageTracker.STORAGE_FILE_DOWNLOAD, storageId, trackerDate);
        } catch (Exception e) {
            LOGGER_FONC.warn("Cannot post Tracker message : non blocking error");
        }

        FileInputStream fileInputStream = null;
        try {
            final String storagePath = this.storageConfigurationBean.getStorageRootDirectory() + File.separator + storedFile.getPath();
            fileInputStream = new FileInputStream(storagePath);
        } catch (FileNotFoundException e) {
            return Response.noContent().build();
        }

        return Response.ok(fileInputStream).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .build();
    }

    /**
     * Returns true if storage tray is accepted.
     * 
     * @param storageTray
     *            Storage tray
     * @return true if accepted
     */
    private boolean isStatusValid(final String storageTray) {
        if (StringUtils.isBlank(storageTray) || !this.storageConfigurationBean.getStorageStatus().contains(storageTray)) {
            LOGGER_FONC.error("Unable to recognize this tray : {}.", storageTray);
            return false;
        }
        return true;
    }

    @Override
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("tray/{storageTray}")
    @ApiOperation(value = "Get all files from a tray")
    public StoredTrayResult get(@ApiParam("Storage tray") @PathParam("storageTray") final String storageTray) {
        StoredTrayResult storedTrayResult = null;
        try {
            if (!isStatusValid(storageTray)) {
                LOGGER_FONC.warn("try {storageTray} don't existe", storageTray);
                return null;
            }

            storedTrayResult = new StoredTrayResult();
            storedTrayResult.setStoredFiles(Optional.ofNullable(this.storageService.getAll(storageTray)).orElse(new ArrayList<StoredFile>()));
            storedTrayResult.setTray(storageTray);

            return storedTrayResult;

        } catch (TechniqueException e) {
            LOGGER_FONC.error("An error occured during getting all files from a tray : {}.", storageTray);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response export(final String storageTray, final String storageId) {
        if (!isStatusValid(storageTray)) {
            LOGGER_FONC.warn("The tray {} does not exist", storageTray);
            return Response.noContent().build();
        }

        try {
            if (!isStatusValid(storageTray)) {
                return Response.status(400).build();
            }
            // -->Download archive record
            final StoredFile storedFile = this.storageService.get(storageId, storageTray);

            // -->Store record into filesystem
            final byte[] recordAsBytes = FileUtils.readFileToByteArray(new File(this.storageConfigurationBean.getStorageRootDirectory() + File.separator + storedFile.getPath()));
            final SpecificationLoader loader = SpecificationLoader.create(new ZipProvider(recordAsBytes));
            this.recordStorage.storeRecord(storageId, recordAsBytes, loader.description().getFormUid());

            // -->Remove file from storage
            this.remove(storageId, storageTray);

            return Response.ok().build();
        } catch (Exception e) {
            LOGGER_FONC.error("Error when exporting record to support", e);
            return Response.noContent().build();
        }
    }
}
