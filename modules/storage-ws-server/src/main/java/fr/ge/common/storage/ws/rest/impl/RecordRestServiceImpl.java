/**
 * 
 */
package fr.ge.common.storage.ws.rest.impl;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.storage.service.IStorageService;
import fr.ge.common.storage.ws.bean.conf.StorageConfigurationBean;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.rest.IRecordRestService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import io.swagger.annotations.Api;

/**
 * Storage Service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api("Storage record REST services")
@Path("/v1/Record")
public class RecordRestServiceImpl implements IRecordRestService {

    /** The storage service. */
    @Autowired
    private IStorageService storageService;

    @Autowired
    private StorageConfigurationBean storageConfigurationBean = StorageConfigurationBean.getInstance();

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<StoredFile> search(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders, final String searchTerms) {
        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders).setSearchTerms(searchTerms);
        return this.storageService.search(searchQuery, StoredFile.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response download(final String storageId) {
        try {
            final StoredFile storedFile = this.storageService.get(storageId);
            final String fileName = Optional.of(storedFile) //
                    .filter(s -> StringUtils.isNotBlank(s.getName())) //
                    .map(s -> s.getName()) //
                    .orElse(storedFile.getId());

            final String storagePath = this.storageConfigurationBean.getStorageRootDirectory() + File.separator + storedFile.getPath();
            final FileInputStream fileInputStream = new FileInputStream(storagePath);

            return Response.ok(fileInputStream) //
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM) //
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"").build();
        } catch (Exception e) {
            return Response.noContent().build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response remove(final List<String> uids, final String fromDate, final List<SearchQueryFilter> filters) {
        if (CollectionUtils.isEmpty(uids) && StringUtils.isEmpty(fromDate) && CollectionUtils.isEmpty(filters)) {
            return Response.noContent().build();
        }

        if (StringUtils.isNotEmpty(fromDate)) {
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            try {
                final LocalDate localDate = LocalDate.parse(fromDate, formatter);
                if (localDate.isEqual(LocalDate.now()) || localDate.isAfter(LocalDate.now())) {
                    return Response.serverError().entity("The date must be different than today and must be before today").build();
                }
            } catch (DateTimeParseException ex) {
                return Response.serverError().entity(String.format("%s must match pattern %s", fromDate, "yyyy-MM-dd")).build();
            }
        }
        final long deleted = this.storageService.remove(uids, fromDate, filters);
        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();
    }

}
