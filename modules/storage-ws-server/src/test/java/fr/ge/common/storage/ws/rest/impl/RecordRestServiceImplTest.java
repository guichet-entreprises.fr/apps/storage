package fr.ge.common.storage.ws.rest.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.storage.service.IStorageService;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 * Unit test for Storage REST service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class RecordRestServiceImplTest extends AbstractRestTest {

    /** The Constant STORAGE_UID. */
    protected static final String STORAGE_UID = "2020-01-STO-RED-01";

    /** The storage service. */
    @Autowired
    private IStorageService storageService;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * Test remove all records.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemoveAll() throws Exception {
        // -->1st test
        // -->prepare
        when(this.storageService.remove(eq(Arrays.asList(STORAGE_UID)), eq(null), eq(Arrays.asList()))).thenReturn(1L);

        // -->call
        Response response = this.client() //
                .path("/v1/Record/remove") //
                .query("uids", Arrays.asList(STORAGE_UID)) //
                .delete();

        // -->verify
        verify(this.storageService).remove(eq(Arrays.asList(STORAGE_UID)), eq(null), eq(Arrays.asList()));
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        // -->2nd test
        // -->prepare
        when(this.storageService.remove(eq(Arrays.asList(STORAGE_UID)), eq(null), eq(Arrays.asList()))).thenReturn(0L);

        // -->call
        response = this.client() //
                .path("/v1/Record/remove") //
                .query("uids", Arrays.asList(STORAGE_UID)) //
                .delete();

        // -->verify
        verify(this.storageService, times(2)).remove(eq(Arrays.asList(STORAGE_UID)), eq(null), eq(Arrays.asList()));
        assertThat(response, hasProperty("status", equalTo(Response.Status.NOT_MODIFIED.getStatusCode())));

        // -->3rd test
        // -->prepare
        final String now = LocalDate.now().toString();
        when(this.storageService.remove(eq(Arrays.asList(STORAGE_UID)), eq(now), eq(Arrays.asList()))).thenReturn(0L);

        // -->call
        response = this.client() //
                .path("/v1/Record/remove") //
                .query("uids", Arrays.asList(STORAGE_UID)) //
                .query("fromDate", now) //
                .delete();

        // -->verify
        verify(this.storageService, never()).remove(eq(Arrays.asList(STORAGE_UID)), eq(now), eq(Arrays.asList()));
        assertThat(response, hasProperty("status", equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())));
    }

}
