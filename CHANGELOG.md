# Changelog
Ce fichier contient les modifications techniques du module.

- Projet : [Storage](https://gitlab.com/guichet-entreprises.fr/apps/storage)

## [2.16.1.0] - 2020-09-11

###  Ajout

- Modifier le logo de la bannière

__Properties__ :

### Modification

| fichier   |      nom      | nouvelle valeur |
|-----------|:-------------:|----------------:|
| ge-gqual-storage-webapp.properties | ui.theme.default | default |

## [2.15.1.4] - 2020-07-08
### Evolution
Exporter une archive au travers d'une API

### Ajout

- __Properties__ 

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-gqual-storage-server.properties | nash.support.directory.input | Input directory to upload record  | /var/data/ge/gent/nashsupport |

## [2.14.2.0] - 2020-03-23
### Evolution

API Suppression physique de dossiers dans Storage

## [2.12.5.0] - 2019-11-20
### Evolution

Ajout API Recherche de dossiers


## [2.12.4.0] - 2019-11-04
### Evolution
Ajout dans liens dans le bandeau

### Ajout

- __Properties__ 

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-gqual-storage-webapp.properties | ui.dashboard.url | URL publique du Dashboard Guichet-Partenaires  | ${DASHBOARD_GP_UI_EXT} |
| ge-gqual-storage-webapp.properties | welcome.public.url | URL publique de Welcome Guichet-Partenaires  | ${WELCOME_GP_UI_EXT} |

## [2.11.11.0] - 2019-08-06


###  Ajout

Correction de la MINE-923 : [RECR] Dossier exporté depuis storage Message Tracker dupliqué (2 fois)

###  Ajout


###  Modification


###  Suppression


__Properties__ :
 
## [2.11.8.0] - 2019-07-18

- Projet : [Storage](https://tools.projet-ge.fr/gitlab/sonic/storage)

###  Ajout

 __Pré-installaton__ :

Créer un répertoire '/var/data/ge/gent/storagedata/partners' contenant les dossiers Backoffice validés par les partenaires
Créer un répertoire '/var/data/ge/gent/nashsupport' contenant les dossiers STORAGE en ERREUR/ENFER pour être importé dans Nash-support

__Properties__ :

Concerne uniquement les applications : storage-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-gqual-storage-webapp.properties | ui.theme.default | Thème par défaut | base-v3 |
| ge-gqual-storage-webapp.properties | ui.theme.defaultSubTheme | Sous-thème par défaut | guichet-partenaires |
| ge-gqual-storage-webapp.properties | environment | Nom de l'environnement | recette rouge |
| ge-gqual-storage-webapp.properties | frontUrl | URL publique du www pour les partenaires | https://www.${env}.guichet-partenaires.fr |
| ge-gqual-storage-webapp.properties | feedbackUrl | URL privée du serveur REST de feedback | https://feedback-ws.${env}.guichet-entreprises.fr/api/public/v1/feedback/widget.js |
| ge-gqual-storage-webapp.properties | mas.storage.service.url | URL privée du serveur REST de storage | http://storage-ws.${env}.guichet-partenaires.loc/api |
| ge-gqual-storage-webapp.properties | nash.support.directory.input | Répertoire de dépôt des dossiers en ERREUR/ENFER pour Nash-support | /var/data/ge/gent/nashsupport |

Concerne uniquement les applications : storage-ws

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-gqual-storage-webapp.properties | ge.gqual.storage.partners.directory | Nom du sous-répertoire des fichiers en archive pour les partenaires | partners |


### Modification


### Suppression


## [3.9.1.0]

- Projet : [Storage](https://tools.projet-ge.fr/gitlab/sonic/storage)

###  Ajout
- add MIT Licence

### Modification


### Suppression



## [2.9.1.0]

- Projet : [Storage](https://tools.projet-ge.fr/gitlab/sonic/storage)

###  Ajout
- remove git history to purge sensitive data

### Modification


### Suppression